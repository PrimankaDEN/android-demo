package dependency

import dependency.Dependency.Configuration
import dependency.Dependency.Variant.Debug
import dependency.Dependency.Variant.Default
import dependency.Dependency.Variant.InstrumentedTest
import dependency.Dependency.Variant.Release
import dependency.Dependency.Variant.UnitTest
import org.gradle.api.Project
import org.gradle.api.artifacts.dsl.DependencyHandler

@Suppress("unused", "MemberVisibilityCanBePrivate")
open class DependencyExtension(private val project: Project) {
    fun setImplementation(groups: List<DependencyGroup>) = set(groups, Configuration.Implementation)
    fun setApi(groups: List<DependencyGroup>) = set(groups, Configuration.Api)
    fun setDefaultConfiguration(groups: List<DependencyGroup>) = set(groups, null)

    private fun set(groups: List<DependencyGroup>, configuration: Configuration?) {
        groups.forEach { group ->
            group.forEach { dependency ->
                project.dependencies.add(dependency, configuration ?: dependency.configuration)
            }
        }
    }

    private fun DependencyHandler.add(
        dependency: Dependency,
        configuration: Configuration
    ) {
        val variant = dependency.variant

        fun notSupported(): Nothing = throw IllegalArgumentException(
            "Combination of $configuration and $variant is not supported; handle it in when statement below"
        )

        fun <T> T.singleItemList(): List<T> = listOf(this)

        val configurationNames = when (configuration) {
            Configuration.Api -> when (variant) {
                Default -> "api"
                UnitTest -> "testImplementation"
                Debug -> "debugApi"
                Release -> "releaseApi"
                else -> notSupported()
            }.singleItemList()
            Configuration.Implementation -> when (variant) {
                Default -> "implementation"
                UnitTest -> "testImplementation"
                InstrumentedTest -> "androidTestImplementation"
                Debug -> "debugImplementation"
                Release -> "releaseImplementation"
            }.singleItemList()
            Configuration.Kapt -> when (variant) {
                // FIXME(https://youtrack.jetbrains.com/issue/KT-21891): Should add `kaptAndroidTest` explicitly
                Default -> listOf("kapt", "kaptAndroidTest")
                UnitTest -> "kaptTest".singleItemList()
                InstrumentedTest -> "kaptAndroidTest".singleItemList()
                else -> notSupported()
            }
            Configuration.CompileOnly -> when (variant) {
                Default -> "compileOnly"
                Release -> "releaseCompileOnly"
                Debug -> "debugCompileOnly"
                else -> notSupported()
            }.singleItemList()
        }

        for (configurationName in configurationNames) {
            add(configurationName, dependency.fullNotation())
        }
    }
}
