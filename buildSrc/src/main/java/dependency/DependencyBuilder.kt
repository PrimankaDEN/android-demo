package dependency

import dependency.Dependency.Configuration.Implementation
import dependency.Dependency.Variant.Default

internal infix fun String.versionOf(artifact: String): List<Dependency> {
    return listOf(DependencyBuilder(this, artifact).build())
}

internal infix fun String.versionOf(fill: DependencyGroupBuilder.() -> Unit): List<Dependency> {
    return DependencyGroupBuilder(this).apply(fill).build()
}

internal class DependencyGroupBuilder(
    private val version: String
) {
    private val dependencies = mutableListOf<Dependency>()

    operator fun String.unaryPlus() {
        dependencies += DependencyBuilder(version, this).build()
    }

    operator fun Dependency.unaryPlus() {
        dependencies += this
    }

    operator fun String.invoke(fill: DependencyBuilder.() -> Unit): Dependency {
        return DependencyBuilder(version, this).apply(fill).build()
    }

    fun build(): DependencyGroup = dependencies
}

internal class DependencyBuilder(
    private val version: String,
    private val artifact: String,
) {
    var variant = Default
    var configuration = Implementation

    fun build() = Dependency(version, artifact, variant, configuration)
}
