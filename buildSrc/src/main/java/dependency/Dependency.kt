package dependency

typealias DependencyGroup = List<Dependency>

class Dependency(
    val version: String,
    val artifact: String,
    val variant: Variant,
    val configuration: Configuration,
) {
    enum class Variant {
        Default,
        UnitTest,
        InstrumentedTest,
        Debug,
        Release,
    }

    enum class Configuration {
        Implementation,
        Api,
        Kapt,
        CompileOnly
    }

    fun fullNotation() = "$artifact:$version"
}
