package dependency

import dependency.Dependency.Configuration.Kapt
import dependency.Dependency.Variant.InstrumentedTest
import dependency.Dependency.Variant.UnitTest

@Suppress("unused", "MemberVisibilityCanBePrivate", "SpellCheckingInspection")
object DependencyHolder {
    const val minSdkVersion = 21
    const val targetSdkVersion = 31
    const val compileSdkVersion = 31
    const val buildToolsVersion = "30.0.2"
    const val gradleBuildToolsVersion = "7.0.4"
    const val kotlinVersion = "1.6.10"

    object Core {
        val appcompat = "1.4.1" versionOf "androidx.appcompat:appcompat"
        val androidxCore = "1.7.0" versionOf "androidx.core:core"
        val androidxCoreKtx = "1.7.0" versionOf "androidx.core:core-ktx"
    }

    object Coroutines {
        val coroutines = "1.6.0" versionOf {
            +"org.jetbrains.kotlinx:kotlinx-coroutines-core"
            +"org.jetbrains.kotlinx:kotlinx-coroutines-android"
            +"org.jetbrains.kotlinx:kotlinx-coroutines-rx2"
            +"org.jetbrains.kotlinx:kotlinx-coroutines-reactive"
            +"org.jetbrains.kotlinx:kotlinx-coroutines-test" { variant = UnitTest }
            +"androidx.lifecycle:lifecycle-viewmodel-ktx"
        }

        val coroutineLifecycleExtensions = "2.4.1" versionOf {
            +"androidx.lifecycle:lifecycle-viewmodel-ktx"
            +"androidx.lifecycle:lifecycle-runtime-ktx"
            +"androidx.lifecycle:lifecycle-livedata-ktx"
        }
    }

    object Store {
        val datastore = "1.0.0" versionOf {
            +"androidx.datastore:datastore-preferences"
        }

        val room = "2.4.2" versionOf {
            +"androidx.room:room-runtime"
            +"androidx.room:room-rxjava2"
            +"androidx.room:room-ktx"
            +"androidx.room:room-compiler" { configuration = Kapt }
            +"androidx.room:room-testing" { variant = UnitTest }
        }
    }

    object Rx {
        val rxJava2 = "2.2.12" versionOf "io.reactivex.rxjava2:rxjava"
        val rxAndroid2 = "2.0.1" versionOf "io.reactivex.rxjava2:rxandroid"
        val rxKotlin2 = "2.2.0" versionOf "io.reactivex.rxjava2:rxkotlin"
        val rxJava2Interop = "0.13.7" versionOf "com.github.akarnokd:rxjava2-interop"

        val rxBinding2Kotlin = "2.0.0" versionOf {
            +"com.jakewharton.rxbinding2:rxbinding-kotlin"
            +"com.jakewharton.rxbinding2:rxbinding-recyclerview-v7-kotlin"
            +"com.jakewharton.rxbinding2:rxbinding-support-v4-kotlin"
        }
    }

    object DI {
        val javaxInject = "1" versionOf "javax.inject:javax.inject"

        val dagger = "2.40.2" versionOf {
            +"com.google.dagger:dagger"
            +"com.google.dagger:dagger-compiler" { configuration = Kapt }
            +"com.google.dagger:dagger-android"
        }

        const val hiltVersion = "2.38.1"
        val hilt = hiltVersion versionOf {
            +"com.google.dagger:hilt-android"
            +"com.google.dagger:hilt-compiler" { configuration = Kapt }
        }
    }

    object UI {
        val recyclerview = "1.1.0" versionOf "androidx.recyclerview:recyclerview"
        val transition = "1.3.1" versionOf "androidx.transition:transition"
        val cardview = "1.0.0" versionOf "androidx.cardview:cardview"
        val viewPager2 = "1.0.0" versionOf "androidx.viewpager2:viewpager2"
        val constraintLayout = "2.0.4" versionOf "androidx.constraintlayout:constraintlayout"
        val drawerlayout = "1.1.1" versionOf "androidx.drawerlayout:drawerlayout"
        val materialComponents = "1.2.0" versionOf "com.google.android.material:material"

        const val composeVersion = "1.1.1"
        val compose = composeVersion versionOf {
            +"androidx.compose.ui:ui"
            +"androidx.compose.material:material"
            +"androidx.compose.ui:ui-tooling"
            +"androidx.compose.runtime:runtime"
            +"androidx.compose.ui:ui-tooling-preview"
        }

        val composeNavigation = "2.4.1" versionOf {
            +"androidx.navigation:navigation-compose"
            +"androidx.navigation:navigation-fragment-ktx"
            +"androidx.navigation:navigation-ui-ktx"
        }
    }

    object Network {
        val okhttp = "4.9.3" versionOf {
            +"com.squareup.okhttp3:okhttp"
            +"com.squareup.okhttp3:logging-interceptor"
            +"com.squareup.okhttp3:mockwebserver" { variant = UnitTest }
        }

        val okio = "3.0.0" versionOf {
            +"com.squareup.okio:okio"
        }

        val retrofit = "2.9.0" versionOf {
            +"com.squareup.retrofit2:retrofit"
            +"com.squareup.retrofit2:converter-moshi"
            +"com.squareup.retrofit2:adapter-rxjava2"
            +"com.squareup.retrofit2:converter-gson"
        }

        val glide = "4.10.0" versionOf {
            +"com.github.bumptech.glide:glide"
            +"com.github.bumptech.glide:okhttp3-integration"
        }

        val ktor = "1.6.8" versionOf {
            +"io.ktor:ktor-client-core"
            +"io.ktor:ktor-client-json"
            +"io.ktor:ktor-client-serialization"
            +"io.ktor:ktor-client-logging"
            +"io.ktor:ktor-client-okhttp"
            +"io.ktor:ktor-client-mock" { variant = UnitTest }
        }
        val ktorLogger = "1.6.1" versionOf "org.slf4j:slf4j-simple"

        val kotlinxSerialization =
            "1.3.2" versionOf "org.jetbrains.kotlinx:kotlinx-serialization-json"
    }

    object Utils {
        val yamlParcer = "2.13.2" versionOf "com.fasterxml.jackson.dataformat:jackson-dataformat-yaml"
    }

    object Tests {
        val junit = "4.12" versionOf { +"junit:junit" { variant = UnitTest } }
        val junitExt =
            "1.1.3" versionOf { +"androidx.test.ext:junit" { variant = InstrumentedTest } }
        val assertJ = "2.8.0" versionOf { +"org.assertj:assertj-core" { variant = UnitTest } }
        val assertParcelable =
            "1.0.1" versionOf {
                +"com.artemzin.assert-parcelable:assert-parcelable" {
                    variant = UnitTest
                }
            }
        val mockito = "3.4.4" versionOf { +"org.mockito:mockito-inline" { variant = UnitTest } }
        val mockitoKotlin =
            "1.5.0" versionOf { +"com.nhaarman:mockito-kotlin" { variant = UnitTest } }
        val mockk = "1.9.3" versionOf { +"io.mockk:mockk" { variant = UnitTest } }
        val strikt = "0.27.0" versionOf { +"io.strikt:strikt-core" { variant = UnitTest } }

        val robolectric = "4.7.3" versionOf {
            +"org.robolectric:robolectric" { variant = UnitTest }
            +"org.robolectric:shadows-multidex" { variant = UnitTest }
        }

        val kotlinTest = kotlinVersion versionOf {
            +"org.jetbrains.kotlin:kotlin-test" { variant = UnitTest }
            +"org.jetbrains.kotlin:kotlin-test-annotations-common" { variant = UnitTest }
            +"org.jetbrains.kotlin:kotlin-test-junit" { variant = UnitTest }
        }
    }

    object Debug {
        val canary = "2.8.1" versionOf {
            +"com.squareup.leakcanary:leakcanary-android" { variant = Dependency.Variant.Debug }
        }
    }
}
