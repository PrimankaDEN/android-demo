plugins {
    `kotlin-dsl`
}

repositories {
    mavenCentral()
}

if (!JavaVersion.current().isJava11Compatible) {
    throw GradleException("This build must be run with Java 11 or greater")
}
