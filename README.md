# Demo Android project #

EVE Online Public API https://esi.evetech.net/ui/?version=latest

### Modules ###
#### serverstatus ####
Provides server status widget
Stack: view/cleanarch/dagger/retrofit/rx/MVP/bindings

#### search ####
Text search form
Stack: fragment/redux/dagger/ktor/flow/MVVM/bindings

#### accounting ####
Provides T1 modules production expense and income information
Stack: fragment/cleanarch/dagger/ktor/flow/MVVM/compose

#### sde ###
Static data export provider
Stack: dagger/ktor/flow
