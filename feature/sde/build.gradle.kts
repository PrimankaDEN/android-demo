import dependency.DependencyHolder.Core
import dependency.DependencyHolder.Coroutines
import dependency.DependencyHolder.DI
import dependency.DependencyHolder.Network
import dependency.DependencyHolder.Store
import dependency.DependencyHolder.Utils
import dependency.DependencyHolder.minSdkVersion
import dependency.DependencyHolder.targetSdkVersion

plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("kotlin-kapt")
    id("kotlinx-serialization")
    id("kotlin-parcelize")
}

externalDependencies {
    setDefaultConfiguration(
        listOf(
            Core.appcompat,
            Core.androidxCore,
            Core.androidxCoreKtx,

            Utils.yamlParcer,

            Coroutines.coroutines,
            Coroutines.coroutineLifecycleExtensions,

            Store.datastore,
            Store.room,

            Network.ktor,
            Network.kotlinxSerialization,
            DI.javaxInject,
            DI.dagger
        )
    )
}

dependencies {
    implementation(project(":common:common"))
    implementation(project(":common:styles"))
    implementation(project(":common:domain"))
}
