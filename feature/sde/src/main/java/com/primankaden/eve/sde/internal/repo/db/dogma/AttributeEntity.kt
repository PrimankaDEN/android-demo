package com.primankaden.eve.sde.internal.repo.db.dogma

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.primanakden.eve.domain.AttributeId
import com.primanakden.eve.domain.AttributeValue
import com.primankaden.common.db.TimestampEntity
import com.primankaden.eve.sde.internal.repo.db.SdeTypeConverters

@Entity(tableName = "attribute")
@TypeConverters(SdeTypeConverters::class)
internal data class AttributeEntity(
    @PrimaryKey
    val attributeId: AttributeId,
    val name: String?,
    val displayName: String?,
    val defaultValue: AttributeValue?,
    override var createdAt: Long = 0,
    override var updatedAt: Long = 0,
) : TimestampEntity
