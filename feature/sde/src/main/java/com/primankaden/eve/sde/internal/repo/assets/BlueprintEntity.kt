package com.primankaden.eve.sde.internal.repo.assets

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

internal data class BlueprintEntity @JsonCreator constructor(
    @JsonProperty("activities") val activities: Activities,
    @JsonProperty("blueprintTypeID") val blueprintTypeID: Int,
    @JsonProperty("maxProductionLimit") val maxProductionLimit: Long,
)

internal data class Activities @JsonCreator constructor(
    @JsonProperty("copying") val copying: Activity?,
    @JsonProperty("manufacturing") val manufacturing: Activity?,
    @JsonProperty("research_material") val researchMaterials: Activity?,
    @JsonProperty("research_time") val researchTime: Activity?,
    @JsonProperty("invention") val invention: Activity?,
    @JsonProperty("reaction") val reaction: Activity?,
)

internal data class Activity @JsonCreator constructor(
    @JsonProperty("materials") val materials: List<Material>?,
    @JsonProperty("products") val products: List<Product>?,
    @JsonProperty("skills") val skills: List<Skill>?,
    @JsonProperty("time") val time: Long,
)

internal data class Material @JsonCreator constructor(
    @JsonProperty("typeID") val typeId: Int,
    @JsonProperty("quantity") val quantity: Long,
)

internal data class Product @JsonCreator constructor(
    @JsonProperty("typeID") val typeId: Int,
    @JsonProperty("quantity") val quantity: Long,
    @JsonProperty("probability") val probability: Int,
)

internal data class Skill @JsonCreator constructor(
    @JsonProperty("typeID") val typeId: Int,
    @JsonProperty("level") val level: Int,
)
