package com.primankaden.eve.sde.api

import com.primanakden.eve.domain.Type
import com.primanakden.eve.domain.TypeId

interface TypeProvider {
    suspend fun getType(typeId: TypeId): Type?
    suspend fun getTypes(typeIds: Set<TypeId>): Map<TypeId, Type?>
}
