package com.primankaden.eve.sde.internal.repo.db.dogma

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.primankaden.common.db.TimestampDao

@Dao
internal interface AttributeDao : TimestampDao<AttributeEntity> {
    @Query("SELECT * FROM attribute WHERE attributeId IS (:id)")
    fun get(id: Int): AttributeEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    override fun insert(entity: AttributeEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    override fun update(entity: AttributeEntity)
}
