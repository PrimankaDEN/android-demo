package com.primankaden.eve.sde.internal.repo

import com.primanakden.eve.domain.Type
import com.primanakden.eve.domain.TypeId
import com.primanakden.eve.domain.TypeWithAttributes
import com.primanakden.eve.domain.deserialize
import com.primankaden.common.cache.MemoryCacheRepo
import com.primankaden.common.db.isActual
import com.primankaden.common.db.updateWithTimestamp
import com.primankaden.common.tools.checkNotMainThread
import com.primankaden.eve.sde.api.TypeProvider
import com.primankaden.eve.sde.internal.di.SdeProviderScope
import com.primankaden.eve.sde.internal.repo.db.type.AttributeValueEntity
import com.primankaden.eve.sde.internal.repo.db.type.TypeDao
import com.primankaden.eve.sde.internal.repo.mappers.toType
import com.primankaden.eve.sde.internal.repo.mappers.toTypeEntity
import com.primankaden.eve.sde.internal.repo.network.SdeApi
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@SdeProviderScope
internal class TypeInfoRepo @Inject constructor(
    private val api: SdeApi,
    private val typeDao: TypeDao,
    private val attributeRepo: AttributeRepo
) : TypeProvider {
    private val cache = MemoryCacheRepo.io(::loadType)

    override suspend fun getType(typeId: TypeId): Type? {
        return cache.get(typeId)
    }

    override suspend fun getTypes(typeIds: Set<TypeId>): Map<TypeId, Type?> {
        return cache.get(typeIds)
    }

    private suspend fun loadType(typeId: TypeId): Type? {
        checkNotMainThread()
        val rawType = loadRawType(typeId) ?: return null
        val attributes = attributeRepo.get(rawType.attributeValues.keys)
        return TypeWithAttributes(rawType, attributes)
    }

    private suspend fun loadRawType(typeId: TypeId): Type? {
        checkNotMainThread()
        val dbEntity = typeDao.getEmbedded(typeId.id)
        if (dbEntity?.typeEntity.isActual(30, TimeUnit.DAYS)) {
            return dbEntity!!.toType()
        }
        // todo error processing
        val response = try {
            api.load(typeId) ?: return null
        } catch (e: Exception) {
            return null
        }
        val entity = response.toTypeEntity()
        typeDao.updateWithTimestamp(entity, dbEntity?.typeEntity)
        response.attributes
            ?.map { AttributeValueEntity(entity.typeId, it.attributeId.deserialize(), it.value) }
            ?.apply { typeDao.insert(this) }
        return response.toType()
    }
}
