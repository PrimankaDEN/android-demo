package com.primankaden.eve.sde.internal.repo.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.primankaden.eve.sde.internal.repo.db.dogma.AttributeDao
import com.primankaden.eve.sde.internal.repo.db.dogma.AttributeEntity
import com.primankaden.eve.sde.internal.repo.db.type.AttributeValueEntity
import com.primankaden.eve.sde.internal.repo.db.type.TypeDao
import com.primankaden.eve.sde.internal.repo.db.type.TypeEntity

@Database(
    entities = [
        TypeEntity::class,
        AttributeEntity::class,
        AttributeValueEntity::class,
    ],
    version = 1
)
internal abstract class SdeDatabase : RoomDatabase() {
    abstract fun typeDao(): TypeDao
    abstract fun attributeDao(): AttributeDao
}
