package com.primankaden.eve.sde.internal.repo.db.type

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import androidx.room.Update
import com.primankaden.common.db.TimestampDao

@Dao
internal interface TypeDao : TimestampDao<TypeEntity> {
    @Query("SELECT * FROM type WHERE typeId IS (:id)")
    fun get(id: Int): TypeEntity?

    @Query("SELECT * FROM type WHERE typeId IS (:id)")
    fun getEmbedded(id: Int): EmbeddedTypeEntity?

    @Query("SELECT * FROM type WHERE typeId IN (:ids)")
    fun get(ids: List<Int>): List<TypeEntity>

    @Insert(onConflict = REPLACE)
    override fun insert(entity: TypeEntity)

    @Update(onConflict = REPLACE)
    override fun update(entity: TypeEntity)

    @Insert(onConflict = REPLACE)
    fun insert(attributes: List<AttributeValueEntity>)
}
