package com.primankaden.eve.sde.internal.repo.network

import com.primanakden.eve.domain.AttributeId
import com.primanakden.eve.domain.TypeId
import com.primanakden.eve.domain.serializeToInt
import com.primankaden.common.network.ApiConfig
import com.primankaden.eve.sde.internal.di.SdeProviderScope
import io.ktor.client.HttpClient
import io.ktor.client.features.expectSuccess
import io.ktor.client.request.get
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import javax.inject.Inject

@SdeProviderScope
internal class SdeApi @Inject constructor(
    private val apiConfig: ApiConfig,
    private val client: HttpClient
) {
    suspend fun load(typeId: TypeId): TypeInfoResponse? {
        return client.get("${apiConfig.baseUrl}universe/types/${typeId.serializeToInt()}/") {
            expectSuccess = false
        }
    }

    suspend fun load(attributeId: AttributeId): AttributeInfoResponse {
        return client.get("${apiConfig.baseUrl}dogma/attributes/${attributeId.serializeToInt()}/")
    }
}

@Serializable
internal data class TypeInfoResponse(
    @SerialName("type_id")
    val typeId: Int,
    val name: String,
    val description: String,
    val volume: Double?,
    @SerialName("dogma_attributes")
    val attributes: List<AttributeValueResponse>? = emptyList()
) {
    @Serializable
    class AttributeValueResponse(
        @SerialName("attribute_id")
        val attributeId: Int,
        val value: Double
    )
}

@Serializable
internal data class AttributeInfoResponse(
    @SerialName("attribute_id")
    val attributeId: Int,
    val name: String? = null,
    @SerialName("display_name")
    val displayName: String? = null,
    val defaultValue: Double? = null
)
