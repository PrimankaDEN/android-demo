package com.primankaden.eve.sde.internal.repo.db.type

import androidx.room.Embedded
import androidx.room.Relation

internal data class EmbeddedTypeEntity(
    @Embedded
    val typeEntity: TypeEntity,
    @Relation(
        parentColumn = "typeId",
        entityColumn = "typeId",
    )
    val attributes: List<AttributeValueEntity>
)
