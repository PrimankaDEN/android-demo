package com.primankaden.eve.sde.internal.repo

import android.content.Context
import android.content.res.AssetManager.ACCESS_STREAMING
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.primanakden.eve.domain.Blueprint
import com.primanakden.eve.domain.BlueprintActivityType
import com.primanakden.eve.domain.TypeId
import com.primankaden.common.tools.checkNotMainThread
import com.primankaden.eve.sde.api.BlueprintProvider
import com.primankaden.eve.sde.internal.di.SdeProviderScope
import com.primankaden.eve.sde.internal.repo.assets.BlueprintEntity
import com.primankaden.eve.sde.internal.repo.mappers.toBlueprint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import javax.inject.Inject

@SdeProviderScope
internal class BlueprintSdeRepo @Inject constructor(
    private val context: Context
) : BlueprintProvider {
    private val mutex = Mutex()

    @Volatile
    private lateinit var blueprintsById: Map<TypeId, Blueprint>

    @Volatile
    private lateinit var blueprintsByProduct: Map<Pair<TypeId, BlueprintActivityType>, Blueprint>

    @Volatile
    private lateinit var blueprints: Set<Blueprint>

    override suspend fun getBlueprints(): Set<Blueprint> {
        loadWithLock()
        return blueprints
    }

    override suspend fun getBlueprintByProduct(productId: TypeId, blueprintActivity: BlueprintActivityType): Blueprint? {
        loadWithLock()
        return blueprintsByProduct[productId to blueprintActivity]
            ?: blueprints.firstOrNull { blueprint ->
                blueprint.activities.values.any { activity ->
                    activity.products.any { it.typeId == productId }
                }
            }
    }

    override suspend fun getBlueprint(blueprintTypeId: TypeId): Blueprint? {
        loadWithLock()
        return blueprintsById[blueprintTypeId]
    }

    private suspend fun loadWithLock() {
        if (!::blueprints.isInitialized) {
            mutex.withLock {
                if (!::blueprints.isInitialized) {
                    prepareIndex(loadBlueprintSde())
                }
            }
        }
    }

    private fun prepareIndex(blueprints: List<Blueprint>) {
        this.blueprints = blueprints.toSet()
        blueprintsById = blueprints.associateBy { it.blueprintId }
//        blueprintsByProduct =  todo
    }

    /**
     *  todo load here https://developers.eveonline.com/resource
     */
    @Suppress("BlockingMethodInNonBlockingContext")
    private suspend fun loadBlueprintSde(): List<Blueprint> {
        return withContext(Dispatchers.IO) {
            checkNotMainThread()
            val reader = context.assets.open("sde/blueprints.yaml", ACCESS_STREAMING).bufferedReader()
            val str = reader.use { it.readText() }
            val factory = YAMLFactory()
            val items = ObjectMapper(factory).readTree(str)
            val mapper = ObjectMapper()
            val result = mutableListOf<Blueprint>()
            for (itemNode: JsonNode in items.elements()) {
                val item = mapper.treeToValue(itemNode, BlueprintEntity::class.java).toBlueprint()
                result.add(item)
            }
            result
        }
    }
}
