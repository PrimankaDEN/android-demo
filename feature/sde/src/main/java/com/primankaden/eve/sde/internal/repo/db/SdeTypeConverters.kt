package com.primankaden.eve.sde.internal.repo.db

import androidx.room.TypeConverter
import com.primanakden.eve.domain.AttributeId
import com.primanakden.eve.domain.TypeId
import com.primanakden.eve.domain.deserialize
import com.primanakden.eve.domain.serializeToInt

class SdeTypeConverters {
    @TypeConverter
    fun convert(id: TypeId): Int = id.serializeToInt()

    @TypeConverter
    fun convertTypeId(id: Int): TypeId = id.deserialize()

    @TypeConverter
    fun convert(id: AttributeId): Int = id.serializeToInt()

    @TypeConverter
    fun convertAttributeId(id: Int): AttributeId = id.deserialize()
}
