package com.primankaden.eve.sde.internal.di

import android.content.Context
import androidx.room.Room
import com.primankaden.eve.sde.api.BlueprintProvider
import com.primankaden.eve.sde.api.SdeDependencies
import com.primankaden.eve.sde.api.SdeProvider
import com.primankaden.eve.sde.api.TypeProvider
import com.primankaden.eve.sde.internal.repo.BlueprintSdeRepo
import com.primankaden.eve.sde.internal.repo.TypeInfoRepo
import com.primankaden.eve.sde.internal.repo.db.SdeDatabase
import com.primankaden.eve.sde.internal.repo.db.dogma.AttributeDao
import com.primankaden.eve.sde.internal.repo.db.type.TypeDao
import dagger.Binds
import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Scope

@Scope
internal annotation class SdeProviderScope

@Component(
    dependencies = [SdeDependencies::class],
    modules = [SdeProviderModule::class, SdeProviderBindsModule::class]
)
@SdeProviderScope
internal interface SdeProviderComponent : SdeProvider {
    @Component.Builder
    interface Builder {
        fun dependencies(serverStatusDependencies: SdeDependencies): Builder

        fun build(): SdeProviderComponent
    }
}

@Module
internal class SdeProviderModule {
    @Provides
    @SdeProviderScope
    fun provideDataBase(context: Context): SdeDatabase {
        return Room.databaseBuilder(
            context,
            SdeDatabase::class.java,
            "sde-database"
        ).build()
    }

    @Provides
    fun provideTypeDao(db: SdeDatabase): TypeDao = db.typeDao()

    @Provides
    fun provideAttributeDao(db: SdeDatabase): AttributeDao = db.attributeDao()
}

@Module
internal interface SdeProviderBindsModule {
    @Binds
    fun bindBlueprintGateway(repo: BlueprintSdeRepo): BlueprintProvider

    @Binds
    fun bindTypeGateway(repo: TypeInfoRepo): TypeProvider
}
