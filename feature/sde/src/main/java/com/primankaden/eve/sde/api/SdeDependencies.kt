package com.primankaden.eve.sde.api

import android.content.Context
import com.primankaden.common.network.ApiConfig
import io.ktor.client.HttpClient

interface SdeDependencies {
    val context: Context
    val apiConfig: ApiConfig
    val client: HttpClient
}
