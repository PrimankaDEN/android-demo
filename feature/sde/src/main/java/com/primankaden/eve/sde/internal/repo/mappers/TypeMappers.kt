package com.primankaden.eve.sde.internal.repo.mappers

import com.primanakden.eve.domain.RawType
import com.primanakden.eve.domain.Type
import com.primanakden.eve.domain.deserialize
import com.primankaden.eve.sde.internal.repo.db.type.EmbeddedTypeEntity
import com.primankaden.eve.sde.internal.repo.db.type.TypeEntity
import com.primankaden.eve.sde.internal.repo.network.TypeInfoResponse

internal fun EmbeddedTypeEntity.toType(): Type {
    return RawType(
        typeEntity.typeId,
        typeEntity.name,
        typeEntity.volume ?: 0.0,
        attributeValues = attributes.associate { it.attributeId to it.value }
    )
}

internal fun TypeInfoResponse.toType(): Type {
    return RawType(
        typeId.deserialize(),
        name,
        volume ?: 0.0
    )
}

internal fun TypeInfoResponse.toTypeEntity(): TypeEntity {
    return TypeEntity(
        typeId.deserialize(),
        name,
        description,
        volume
    )
}
