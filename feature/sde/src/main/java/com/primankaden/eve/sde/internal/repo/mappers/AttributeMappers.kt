package com.primankaden.eve.sde.internal.repo.mappers

import com.primanakden.eve.domain.Attribute
import com.primanakden.eve.domain.deserialize
import com.primankaden.eve.sde.internal.repo.db.dogma.AttributeEntity
import com.primankaden.eve.sde.internal.repo.network.AttributeInfoResponse

internal fun AttributeEntity.toAttribute(): Attribute {
    return Attribute(
        attributeId,
        displayName,
        defaultValue
    )
}

internal fun AttributeInfoResponse.toAttributeEntity(): AttributeEntity {
    return AttributeEntity(
        attributeId.deserialize(),
        name,
        displayName,
        defaultValue
    )
}

internal fun AttributeInfoResponse.toAttribute(): Attribute {
    return Attribute(
        attributeId.deserialize(),
        displayName,
        defaultValue
    )
}
