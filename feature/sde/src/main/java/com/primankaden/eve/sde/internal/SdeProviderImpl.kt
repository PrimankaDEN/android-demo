package com.primankaden.eve.sde.internal

import com.primankaden.eve.sde.api.BlueprintProvider
import com.primankaden.eve.sde.api.SdeDependencies
import com.primankaden.eve.sde.api.SdeProvider
import com.primankaden.eve.sde.api.TypeProvider
import com.primankaden.eve.sde.internal.di.DaggerSdeProviderComponent

internal class SdeProviderImpl(
    dependencies: SdeDependencies
) : SdeProvider {
    private val component by lazy {
        DaggerSdeProviderComponent.builder()
            .dependencies(dependencies)
            .build()
    }

    override fun typeProvider(): TypeProvider = component.typeProvider()

    override fun blueprintProvider(): BlueprintProvider = component.blueprintProvider()
}
