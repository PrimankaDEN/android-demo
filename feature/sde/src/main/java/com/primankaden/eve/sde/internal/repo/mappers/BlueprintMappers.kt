package com.primankaden.eve.sde.internal.repo.mappers

import com.primanakden.eve.domain.Blueprint
import com.primanakden.eve.domain.BlueprintActivity
import com.primanakden.eve.domain.BlueprintActivityType
import com.primanakden.eve.domain.BlueprintActivityType.Copying
import com.primanakden.eve.domain.BlueprintActivityType.Invention
import com.primanakden.eve.domain.BlueprintActivityType.Manufacturing
import com.primanakden.eve.domain.BlueprintActivityType.Reaction
import com.primanakden.eve.domain.BlueprintActivityType.ResearchMaterials
import com.primanakden.eve.domain.BlueprintActivityType.ResearchTime
import com.primanakden.eve.domain.BlueprintMaterial
import com.primanakden.eve.domain.BlueprintProduct
import com.primanakden.eve.domain.deserialize
import com.primankaden.eve.sde.internal.repo.assets.Activity
import com.primankaden.eve.sde.internal.repo.assets.BlueprintEntity
import com.primankaden.eve.sde.internal.repo.assets.Material
import com.primankaden.eve.sde.internal.repo.assets.Product

internal fun BlueprintEntity.toBlueprint(): Blueprint {
    return Blueprint(
        blueprintTypeID.deserialize(),
        listOfNotNull(
            activities.copying?.toBlueprintActivity(Copying),
            activities.manufacturing?.toBlueprintActivity(Manufacturing),
            activities.researchMaterials?.toBlueprintActivity(ResearchMaterials),
            activities.researchTime?.toBlueprintActivity(ResearchTime),
            activities.invention?.toBlueprintActivity(Invention),
            activities.reaction?.toBlueprintActivity(Reaction)
        ).associateBy { it.typeBlueprint }
    )
}

internal fun Activity.toBlueprintActivity(typeBlueprint: BlueprintActivityType): BlueprintActivity {
    val materials = this.materials?.map { it.toBlueprintMaterial() }?.toSet() ?: emptySet()
    val product = this.products?.map { it.toBlueprintProduct() }?.toSet() ?: emptySet()
    return BlueprintActivity(typeBlueprint, materials, product, time)
}

internal fun Material.toBlueprintMaterial(): BlueprintMaterial {
    return BlueprintMaterial(
        typeId.deserialize(),
        quantity.toInt()
    )
}

internal fun Product.toBlueprintProduct(): BlueprintProduct {
    return BlueprintProduct(
        typeId.deserialize(),
        quantity.toInt(),
        probability.toDouble()
    )
}
