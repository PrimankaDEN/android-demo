package com.primankaden.eve.sde.api

import com.primanakden.eve.domain.Blueprint
import com.primanakden.eve.domain.BlueprintActivityType
import com.primanakden.eve.domain.TypeId

interface BlueprintProvider {
    suspend fun getBlueprint(blueprintTypeId: TypeId): Blueprint?
    suspend fun getBlueprintByProduct(productId: TypeId, blueprintActivity: BlueprintActivityType): Blueprint?
    suspend fun getBlueprints(): Set<Blueprint>
}
