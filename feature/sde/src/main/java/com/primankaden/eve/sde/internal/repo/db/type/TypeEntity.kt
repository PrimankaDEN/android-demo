package com.primankaden.eve.sde.internal.repo.db.type

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.primanakden.eve.domain.TypeId
import com.primanakden.eve.domain.Volume
import com.primankaden.common.db.TimestampEntity
import com.primankaden.eve.sde.internal.repo.db.SdeTypeConverters

@Entity(tableName = "type")
@TypeConverters(SdeTypeConverters::class)
internal data class TypeEntity(
    @PrimaryKey
    val typeId: TypeId,
    val name: String,
    val description: String,
    val volume: Volume?,
    override var createdAt: Long = 0,
    override var updatedAt: Long = 0,
) : TimestampEntity
