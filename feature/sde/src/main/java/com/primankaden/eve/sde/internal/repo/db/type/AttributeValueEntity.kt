package com.primankaden.eve.sde.internal.repo.db.type

import androidx.room.Entity
import androidx.room.TypeConverters
import com.primanakden.eve.domain.AttributeId
import com.primanakden.eve.domain.AttributeValue
import com.primanakden.eve.domain.TypeId
import com.primankaden.eve.sde.internal.repo.db.SdeTypeConverters

@Entity(primaryKeys = ["typeId", "attributeId"], tableName = "attributeValue")
@TypeConverters(SdeTypeConverters::class)
internal data class AttributeValueEntity(
    val typeId: TypeId,
    val attributeId: AttributeId,
    val value: AttributeValue
)
