package com.primankaden.eve.sde.api

import com.primankaden.eve.sde.internal.SdeProviderImpl

interface SdeProvider {
    fun typeProvider(): TypeProvider
    fun blueprintProvider(): BlueprintProvider

    class Builder {
        private lateinit var dependencies: SdeDependencies

        fun dependencies(dependencies: SdeDependencies): Builder {
            this.dependencies = dependencies
            return this
        }

        fun build(): SdeProvider {
            return SdeProviderImpl(dependencies)
        }
    }
}
