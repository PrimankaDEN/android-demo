package com.primankaden.eve.sde.internal.repo

import com.primanakden.eve.domain.Attribute
import com.primanakden.eve.domain.AttributeId
import com.primanakden.eve.domain.serializeToInt
import com.primankaden.common.cache.MemoryCacheRepo
import com.primankaden.common.db.isActual
import com.primankaden.common.db.updateWithTimestamp
import com.primankaden.common.tools.checkNotMainThread
import com.primankaden.eve.sde.internal.di.SdeProviderScope
import com.primankaden.eve.sde.internal.repo.db.dogma.AttributeDao
import com.primankaden.eve.sde.internal.repo.mappers.toAttribute
import com.primankaden.eve.sde.internal.repo.mappers.toAttributeEntity
import com.primankaden.eve.sde.internal.repo.network.SdeApi
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@SdeProviderScope
internal class AttributeRepo @Inject constructor(
    private val api: SdeApi,
    private val attributeDao: AttributeDao,
) {
    private val cache = MemoryCacheRepo.io(::loadAttribute)

    suspend fun get(attributeId: AttributeId) = cache.get(attributeId)

    suspend fun get(attributeIds: Set<AttributeId>) = cache.get(attributeIds)

    private suspend fun loadAttribute(id: AttributeId): Attribute {
        checkNotMainThread()
        val dbEntity = attributeDao.get(id.serializeToInt())
        if (dbEntity.isActual(30, TimeUnit.DAYS)) {
            return dbEntity!!.toAttribute()
        }
        val response = api.load(id)
        attributeDao.updateWithTimestamp(response.toAttributeEntity(), dbEntity)
        return response.toAttribute()
    }
}
