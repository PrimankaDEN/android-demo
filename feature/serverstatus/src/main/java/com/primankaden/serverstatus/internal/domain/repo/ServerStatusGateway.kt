package com.primankaden.serverstatus.internal.domain.repo

import com.primankaden.serverstatus.internal.domain.entity.ServerStatus
import io.reactivex.Observable

internal interface ServerStatusGateway {
    fun observeStatus(): Observable<ServerStatus>
}
