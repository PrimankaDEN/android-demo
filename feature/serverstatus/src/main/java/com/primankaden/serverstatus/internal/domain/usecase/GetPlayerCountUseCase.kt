package com.primankaden.serverstatus.internal.domain.usecase

import android.content.Context
import com.primankaden.serverstatus.R
import com.primankaden.serverstatus.internal.domain.entity.ServerStatus
import com.primankaden.serverstatus.internal.domain.repo.ServerStatusGateway
import io.reactivex.Observable
import javax.inject.Inject

internal class GetPlayerCountUseCase @Inject constructor(
    private val context: Context,
    private val statusGateway: ServerStatusGateway
) {
    operator fun invoke(): Observable<String> {
        val placeholder = { context.getString(R.string.no_players_placeholder) }
        return statusGateway.observeStatus()
            .map {
                if (it is ServerStatus.Online) "%,d".format(it.players) else placeholder()
            }
    }
}
