package com.primankaden.serverstatus.internal.di

import android.content.Context
import com.primankaden.common.network.ApiConfig
import com.primankaden.serverstatus.api.ServerStatusDependencies
import com.primankaden.serverstatus.api.ServerStatusWidget
import com.primankaden.serverstatus.internal.data.ServerStatusRepo
import com.primankaden.serverstatus.internal.data.StatusApi
import com.primankaden.serverstatus.internal.domain.repo.ServerStatusGateway
import dagger.Binds
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Scope

@Scope
internal annotation class ServerStatusViewScope

@ServerStatusViewScope
@Component(
    dependencies = [ServerStatusDependencies::class],
    modules = [
        ServerStatusModule::class,
        ServerStatusBindModule::class
    ]
)
internal interface ServerStatusComponent {
    fun inject(view: ServerStatusWidget)

    @Component.Builder
    interface Builder {
        fun dependencies(serverStatusDependencies: ServerStatusDependencies): Builder

        @BindsInstance
        fun context(context: Context): Builder

        fun build(): ServerStatusComponent
    }
}

@Module
internal object ServerStatusModule {
    @Provides
    @ServerStatusViewScope
    fun provideStatusApi(apiConfig: ApiConfig, okHttpClient: OkHttpClient): StatusApi {
        val retrofit = Retrofit.Builder()
            .baseUrl(apiConfig.baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
        return retrofit.create(StatusApi::class.java)
    }
}

@Module
internal interface ServerStatusBindModule {
    @Binds
    fun bindStatusGateway(repo: ServerStatusRepo): ServerStatusGateway
}
