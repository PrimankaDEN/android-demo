package com.primankaden.serverstatus.internal.presentation

import com.primankaden.serverstatus.internal.domain.usecase.GetPlayerCountUseCase
import com.primankaden.serverstatus.internal.domain.usecase.GetStatusIconUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposables
import io.reactivex.rxkotlin.Observables
import javax.inject.Inject

internal class ServerStatusPresenter @Inject constructor(
    private val getStatusColor: GetStatusIconUseCase,
    private val getPlayerCount: GetPlayerCountUseCase,
) {
    private var view: ServerStatusView? = null
    private var disposable = Disposables.disposed()

    fun attach(view: ServerStatusView) {
        this.view = view
        disposable = Observables.combineLatest(
            getStatusColor(),
            getPlayerCount()
        )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { (icon, count) ->
                view.updateStatus(icon, count)
            }
    }

    fun detach() {
        this.view = null
        disposable.dispose()
    }
}
