package com.primankaden.serverstatus.internal.data

import com.google.gson.annotations.SerializedName
import io.reactivex.Single
import retrofit2.http.GET

internal interface StatusApi {
    /**
     * https://esi.evetech.net/ui/?version=latest#/Status/get_status
     */
    @GET(value = "status")
    fun getStatus(): Single<ServerStatusResponse>
}

internal class ServerStatusResponse(
    @SerializedName("players")
    val players: Int,
    @SerializedName("server_version")
    val serverVersion: String,
    @SerializedName("start_time")
    val startTime: String
) {
    companion object {
        const val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    }
}
