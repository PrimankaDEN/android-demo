package com.primankaden.serverstatus.internal.data

import com.primankaden.serverstatus.internal.di.ServerStatusViewScope
import com.primankaden.serverstatus.internal.domain.entity.ServerStatus
import com.primankaden.serverstatus.internal.domain.repo.ServerStatusGateway
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

private const val INTERVAL_SEC = 30L

@ServerStatusViewScope
internal class ServerStatusRepo @Inject constructor(
    api: StatusApi
) : ServerStatusGateway {
    private val apiRequest = api.getStatus().map { it.map() }.onErrorReturn { ServerStatus.Offline }

    private val status = Observable.concat(
        apiRequest.toObservable(),
        Observable.interval(INTERVAL_SEC, TimeUnit.SECONDS)
            .flatMapSingle { apiRequest }
    )
        .startWith(ServerStatus.Loading)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .replay(1)
        .refCount()

    override fun observeStatus(): Observable<ServerStatus> {
        return status.hide()
    }
}

private fun ServerStatusResponse.map(): ServerStatus {
    return if (players == 0) ServerStatus.Offline else ServerStatus.Online(players)
}
