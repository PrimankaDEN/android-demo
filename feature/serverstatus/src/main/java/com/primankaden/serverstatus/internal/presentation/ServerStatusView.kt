package com.primankaden.serverstatus.internal.presentation

import androidx.annotation.DrawableRes

internal interface ServerStatusView {
    fun updateStatus(@DrawableRes icon: Int, text: String)
}
