package com.primankaden.serverstatus.api

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.appcompat.content.res.AppCompatResources
import com.primankaden.common.di.findComponentDependencies
import com.primankaden.serverstatus.R
import com.primankaden.serverstatus.databinding.ViewServerStatusBinding
import com.primankaden.serverstatus.internal.di.DaggerServerStatusComponent
import com.primankaden.serverstatus.internal.presentation.ServerStatusPresenter
import com.primankaden.serverstatus.internal.presentation.ServerStatusView
import javax.inject.Inject

class ServerStatusWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {
    private var binding = ViewServerStatusBinding.inflate(LayoutInflater.from(context), this)

    init {
        background = AppCompatResources.getDrawable(context, R.drawable.background_status)
    }

    @Suppress("unused")
    private val component = DaggerServerStatusComponent.builder()
        .dependencies(findComponentDependencies())
        .context(context)
        .build()
        .also { it.inject(this) }

    @Inject
    internal lateinit var presenter: ServerStatusPresenter

    private val view = object : ServerStatusView {
        override fun updateStatus(icon: Int, text: String) {
            binding.apply {
                statusView.background = AppCompatResources.getDrawable(context, icon)
                playerCountView.text = text
            }
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        presenter.attach(view)
    }

    override fun onDetachedFromWindow() {
        presenter.detach()
        super.onDetachedFromWindow()
    }
}
