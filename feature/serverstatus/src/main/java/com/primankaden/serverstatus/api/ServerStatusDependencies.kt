package com.primankaden.serverstatus.api

import com.primankaden.common.di.ComponentDependencies
import com.primankaden.common.network.ApiConfig
import okhttp3.OkHttpClient

interface ServerStatusDependencies : ComponentDependencies {
    val apiConfig: ApiConfig
    val okHttpClient: OkHttpClient
}
