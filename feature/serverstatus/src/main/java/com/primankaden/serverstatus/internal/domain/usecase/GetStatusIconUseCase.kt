package com.primankaden.serverstatus.internal.domain.usecase

import com.primankaden.serverstatus.R
import com.primankaden.serverstatus.internal.domain.entity.ServerStatus
import com.primankaden.serverstatus.internal.domain.repo.ServerStatusGateway
import io.reactivex.Observable
import javax.inject.Inject

internal class GetStatusIconUseCase @Inject constructor(private val statusGateway: ServerStatusGateway) {
    operator fun invoke(): Observable<Int> {
        return statusGateway.observeStatus()
            .map {
                when (it) {
                    ServerStatus.Loading -> R.drawable.icon_status_loading
                    is ServerStatus.Online -> R.drawable.icon_status_online
                    is ServerStatus.Offline -> R.drawable.icon_status_offline
                }
            }
    }
}
