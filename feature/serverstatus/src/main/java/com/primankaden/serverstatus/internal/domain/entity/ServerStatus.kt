package com.primankaden.serverstatus.internal.domain.entity

internal typealias PlayerCount = Int

internal sealed class ServerStatus {
    object Loading : ServerStatus()
    class Online(val players: PlayerCount) : ServerStatus()
    object Offline : ServerStatus()
}
