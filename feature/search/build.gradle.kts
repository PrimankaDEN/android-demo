import dependency.DependencyHolder.Core
import dependency.DependencyHolder.Coroutines
import dependency.DependencyHolder.DI
import dependency.DependencyHolder.Network
import dependency.DependencyHolder.UI

plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("kotlin-kapt")
    id("kotlinx-serialization")
    id("kotlin-parcelize")
}

android {
    buildFeatures {
        viewBinding = true
    }
}

externalDependencies {
    setDefaultConfiguration(
        listOf(
            Core.appcompat,
            Core.androidxCore,
            Core.androidxCoreKtx,

            Coroutines.coroutines,
            Coroutines.coroutineLifecycleExtensions,

            UI.recyclerview,

            Network.ktor,
            Network.kotlinxSerialization,
            DI.javaxInject,
            DI.dagger
        )
    )
}

dependencies {
    implementation(project(":common:common"))
    implementation(project(":common:domain"))
    implementation(project(":common:redux"))
    implementation(project(":common:styles"))
}
