package com.primankaden.search.internal.redux.state

import android.os.Parcelable
import com.primanakden.eve.domain.Type
import com.primanakden.eve.domain.TypeId
import com.primankaden.common.redux.Store
import com.primankaden.search.internal.redux.reducer.SearchAction
import kotlinx.parcelize.Parcelize

internal typealias SearchStore = Store<SearchState, SearchAction>

internal typealias SearchTerm = String

@Parcelize
internal data class SearchState(
    val searchState: SearchListState,
    val favoriteState: FavoriteListState,
) : Parcelable {
    companion object {
        fun initialState() = SearchState(
            SearchListState(null, null, emptyMap()),
            FavoriteListState(emptySet(), emptyMap())
        )
    }
}

@Parcelize
internal data class SearchListState(
    val searchTerm: SearchTerm?,
    val searchRequestData: SearchRequestData?,
    val types: Map<TypeId, Type?>,
) : Parcelable

@Parcelize
internal data class FavoriteListState(
    val favoriteIds: Set<TypeId>,
    val types: Map<TypeId, Type?>,
) : Parcelable
