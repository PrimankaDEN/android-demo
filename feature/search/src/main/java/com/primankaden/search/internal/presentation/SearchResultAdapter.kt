package com.primankaden.search.internal.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.primankaden.search.R
import com.primankaden.search.databinding.ViewHolderSearchItemBinding

internal class SearchResultAdapter(
    private val clickListener: (SearchResultItem) -> Unit
) : RecyclerView.Adapter<SearchResultViewHolder>() {
    var data: List<SearchResultItem> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = SearchResultViewHolder(parent, clickListener)

    override fun onBindViewHolder(holder: SearchResultViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount() = data.size
}

internal class SearchResultViewHolder(
    parent: ViewGroup,
    private val clickListener: (SearchResultItem) -> Unit
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.view_holder_search_item, parent, false)
) {
    private val binding = ViewHolderSearchItemBinding.bind(itemView)

    fun bind(item: SearchResultItem) {
        binding.apply {
            title.text = item.title
            favoriteIcon.isVisible = item.favorite
            root.setOnClickListener { clickListener(item) }
        }
    }
}
