package com.primankaden.search.internal.redux.epics

import com.primanakden.eve.domain.Type
import com.primanakden.eve.domain.TypeId
import com.primanakden.eve.domain.deserialize
import com.primankaden.common.redux.Epic
import com.primankaden.search.api.TypeProvider
import com.primankaden.search.internal.redux.reducer.SearchAction
import com.primankaden.search.internal.redux.reducer.SetFavoriteTypeAction
import com.primankaden.search.internal.redux.reducer.SetSearchTypeAction
import com.primankaden.search.internal.redux.state.SearchRequestData
import com.primankaden.search.internal.redux.state.SearchState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.merge
import javax.inject.Inject

internal class TypeRequestEpic @Inject constructor(
    private val typeProvider: TypeProvider
) : Epic<SearchState, SearchAction> {
    override fun act(states: Flow<SearchState>, actions: Flow<SearchAction>): Flow<SearchAction> {
        return merge(
            requestSearchType(states),
            requestFavoriteType(states)
        )
    }

    private fun requestSearchType(states: Flow<SearchState>): Flow<SearchAction> {
        return states
            .map { it.searchState }
            .filter { it.searchRequestData is SearchRequestData.Success && it.types.isEmpty() }
            .distinctUntilChanged { old, new -> old.searchRequestData == new.searchRequestData }
            .flatMapLatest { state ->
                (state.searchRequestData as SearchRequestData.Success)
                    .response.inventoryTypeIds
                    .map { it.deserialize<TypeId>() }
                    .requestItemFlows { id, type -> SetSearchTypeAction(id, type) }
                    .merge()
            }
    }

    private fun requestFavoriteType(states: Flow<SearchState>): Flow<SearchAction> {
        return states
            .map { it.favoriteState }
            .filter { it.types.isEmpty() }
            .distinctUntilChanged { old, new -> old.favoriteIds == new.favoriteIds }
            .flatMapLatest { state ->
                state.favoriteIds
                    .requestItemFlows { id, type -> SetFavoriteTypeAction(id, type) }
                    .merge()
            }
    }

    private fun Iterable<TypeId>.requestItemFlows(
        action: (TypeId, Type?) -> SearchAction
    ): Iterable<Flow<SearchAction>> {
        return map { id -> (suspend { action(id, typeProvider.getType(id)) }).asFlow() }
    }
}
