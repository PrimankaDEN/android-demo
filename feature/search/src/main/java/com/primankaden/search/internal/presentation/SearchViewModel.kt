package com.primankaden.search.internal.presentation

import com.primanakden.eve.domain.TypeId
import com.primanakden.eve.domain.deserialize
import com.primankaden.search.internal.redux.reducer.AddFavoriteTypeAction
import com.primankaden.search.internal.redux.reducer.ClearSearchTermAction
import com.primankaden.search.internal.redux.reducer.RemoveFavoriteTypeAction
import com.primankaden.search.internal.redux.reducer.SearchAction
import com.primankaden.search.internal.redux.reducer.SetSearchTermAction
import com.primankaden.search.internal.redux.state.FavoriteListState
import com.primankaden.search.internal.redux.state.SearchListState
import com.primankaden.search.internal.redux.state.SearchRequestData
import com.primankaden.search.internal.redux.state.SearchStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.mapLatest
import javax.inject.Inject

internal class SearchViewModel @Inject constructor(
    private val store: SearchStore,
) {
    val resultItems: Flow<SearchViewState> = store.states()
        .mapLatest { state ->
            if (state.searchState.searchRequestData == null) {
                favoriteList(state.favoriteState)
            } else {
                searchList(state.searchState, state.favoriteState.favoriteIds)
            }
        }
        .distinctUntilChanged()

    private fun favoriteList(state: FavoriteListState): SearchViewState {
        return state.run {
            val list = favoriteIds
                .mapNotNull { types[it] }
                .map {
                    SearchResultItem(
                        it.title,
                        true,
                        RemoveFavoriteTypeAction(it.typeId)
                    )
                }
            if (list.isEmpty()) {
                SearchViewState.Placeholder("No favorites")
            } else {
                SearchViewState.Result(list)
            }
        }
    }

    private fun searchList(state: SearchListState, favorites: Set<TypeId>): SearchViewState {
        return state.run {
            when (searchRequestData) {
                is SearchRequestData.Loading -> SearchViewState.Placeholder("Loading")
                is SearchRequestData.Error -> SearchViewState.Placeholder(searchRequestData.error.toString())
                is SearchRequestData.Success -> {
                    val list = searchRequestData.response.inventoryTypeIds
                        .map { it.deserialize<TypeId>() }
                        .mapNotNull { types[it] }
                        .map {
                            val favorite = favorites.contains(it.typeId)
                            SearchResultItem(
                                it.title,
                                favorite,
                                if (favorite) RemoveFavoriteTypeAction(it.typeId) else AddFavoriteTypeAction(it.typeId)
                            )
                        }

                    if (list.isEmpty()) {
                        SearchViewState.Placeholder("No favorites")
                    } else {
                        SearchViewState.Result(list)
                    }
                }
                else -> throw IllegalStateException()
            }
        }
    }

    fun setSearchTermChanges(term: String) {
        store.dispatch(SetSearchTermAction(term))
    }

    fun clearSearchTerm() {
        store.dispatch(ClearSearchTermAction)
    }

    fun clickItem(item: SearchResultItem) {
        store.dispatch(item.clickAction)
    }
}

internal sealed class SearchViewState {
    data class Placeholder(val text: String) : SearchViewState()
    data class Result(val list: List<SearchResultItem>) : SearchViewState()
}

internal data class SearchResultItem(
    val title: String,
    val favorite: Boolean,
    val clickAction: SearchAction
)
