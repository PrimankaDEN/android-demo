package com.primankaden.search.api

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.primankaden.common.di.findComponentDependencies
import com.primankaden.common.extensions.args
import com.primankaden.common.extensions.changes
import com.primankaden.common.extensions.getValue
import com.primankaden.common.extensions.launchWhenStarted
import com.primankaden.common.extensions.setValue
import com.primankaden.common.tools.RefWatcher
import com.primankaden.search.databinding.FragmentSearchBinding
import com.primankaden.search.internal.di.DaggerSearchComponent
import com.primankaden.search.internal.presentation.SearchResultAdapter
import com.primankaden.search.internal.presentation.SearchViewModel
import com.primankaden.search.internal.presentation.SearchViewState
import com.primankaden.search.internal.redux.epics.EpicRegisterer
import com.primankaden.search.internal.redux.state.SearchState
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

class SearchFragment : Fragment() {
    @Suppress("unused")
    private val component by lazy {
        DaggerSearchComponent.builder()
            .dependencies(findComponentDependencies())
            .initialState(SearchState.initialState())
            .context(requireContext())
            .scope(lifecycleScope)
            .build()
    }

    @Inject
    internal lateinit var viewModel: SearchViewModel

    @Inject
    internal lateinit var watcher: RefWatcher

    @Inject
    internal lateinit var epicRegisterer: EpicRegisterer

    private var stateBackup: SearchState? by args
    private lateinit var binding: FragmentSearchBinding
    private val itemAdapter = SearchResultAdapter { viewModel.clickItem(it) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        component.inject(this)
        epicRegisterer.register()

        binding.result.apply {
            adapter = itemAdapter
            layoutManager = LinearLayoutManager(context)
        }
        viewModel.resultItems
            .onEach {
                itemAdapter.data = (it as? SearchViewState.Result)?.list ?: emptyList()
            }
            .launchWhenStarted(lifecycleScope)
        binding.search.changes()
            .distinctUntilChanged()
            .onEach { viewModel.setSearchTermChanges(it) }
            .launchWhenStarted(lifecycleScope)
    }

    override fun onDestroy() {
        super.onDestroy()
        watcher.watchRef(component)
        watcher.watchRef(viewModel)
    }
}
