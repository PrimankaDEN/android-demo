package com.primankaden.search.api

import com.primanakden.eve.domain.Type
import com.primanakden.eve.domain.TypeId
import com.primankaden.common.di.ComponentDependencies
import com.primankaden.common.network.ApiConfig
import com.primankaden.common.tools.RefWatcher
import io.ktor.client.HttpClient
import kotlinx.coroutines.flow.Flow

interface SearchDependencies : ComponentDependencies {
    val apiConfig: ApiConfig
    val client: HttpClient
    val favoritesProvider: FavoritesProvider
    val watcher: RefWatcher
    val searchTypeProvider: TypeProvider
}

interface FavoritesProvider {
    suspend fun addFavorite(id: TypeId)
    suspend fun removeFavorite(id: TypeId)
    fun favorites(): Flow<Set<TypeId>>
}

interface TypeProvider {
    suspend fun getType(id: TypeId): Type?
}
