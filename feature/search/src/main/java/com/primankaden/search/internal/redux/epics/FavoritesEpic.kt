package com.primankaden.search.internal.redux.epics

import com.primankaden.common.extensions.ofType
import com.primankaden.common.extensions.skipAll
import com.primankaden.common.redux.Epic
import com.primankaden.search.api.FavoritesProvider
import com.primankaden.search.internal.redux.reducer.AddFavoriteTypeAction
import com.primankaden.search.internal.redux.reducer.RemoveFavoriteTypeAction
import com.primankaden.search.internal.redux.reducer.SearchAction
import com.primankaden.search.internal.redux.reducer.SetFavoriteTypesAction
import com.primankaden.search.internal.redux.state.SearchState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

internal class FavoritesEpic @Inject constructor(
    private val favoritesProvider: FavoritesProvider,
) : Epic<SearchState, SearchAction> {
    override fun act(states: Flow<SearchState>, actions: Flow<SearchAction>): Flow<SearchAction> {
        return merge(
            favoritesProvider.favorites().map { SetFavoriteTypesAction(it) },
            actions.ofType<AddFavoriteTypeAction>().onEach { favoritesProvider.addFavorite(it.id) }.skipAll(),
            actions.ofType<RemoveFavoriteTypeAction>().onEach { favoritesProvider.removeFavorite(it.id) }.skipAll(),
        )
    }
}
