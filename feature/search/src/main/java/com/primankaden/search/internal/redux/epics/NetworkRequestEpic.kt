package com.primankaden.search.internal.redux.epics

import com.primankaden.common.redux.Epic
import com.primankaden.search.internal.network.SearchApi
import com.primankaden.search.internal.redux.reducer.SearchAction
import com.primankaden.search.internal.redux.reducer.SetSearchRequestResultAction
import com.primankaden.search.internal.redux.state.SearchRequestData
import com.primankaden.search.internal.redux.state.SearchRequestError
import com.primankaden.search.internal.redux.state.SearchState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapLatest
import javax.inject.Inject

internal class NetworkRequestEpic @Inject constructor(
    private val api: SearchApi,
) : Epic<SearchState, SearchAction> {
    override fun act(states: Flow<SearchState>, actions: Flow<SearchAction>): Flow<SearchAction> {
        return requestSearch(states)
    }

    private fun requestSearch(states: Flow<SearchState>): Flow<SearchAction> {
        return states
            .map { it.searchState.searchRequestData }
            .filter { it != null && it is SearchRequestData.Loading }
            .map { it!!.request }
            .distinctUntilChanged()
            .mapLatest { request ->
                SetSearchRequestResultAction(
                    try {
                        val response = api.search(request)
                        SearchRequestData.Success(request, response)
                    } catch (e: Exception) {
                        SearchRequestData.Error(request, SearchRequestError.UnknownError)
                    }
                )
            }
    }
}
