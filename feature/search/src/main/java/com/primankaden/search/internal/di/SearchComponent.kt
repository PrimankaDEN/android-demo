package com.primankaden.search.internal.di

import android.content.Context
import com.primankaden.common.redux.EpicMiddleware
import com.primankaden.common.redux.LoggingMiddleware
import com.primankaden.common.redux.Store
import com.primankaden.search.api.SearchDependencies
import com.primankaden.search.api.SearchFragment
import com.primankaden.search.internal.redux.epics.FavoritesEpic
import com.primankaden.search.internal.redux.epics.NetworkRequestEpic
import com.primankaden.search.internal.redux.epics.TypeRequestEpic
import com.primankaden.search.internal.redux.reducer.SearchAction
import com.primankaden.search.internal.redux.reducer.reduce
import com.primankaden.search.internal.redux.state.SearchState
import com.primankaden.search.internal.redux.state.SearchStore
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineScope
import javax.inject.Scope

@Scope
internal annotation class SearchFragmentScope

@Component(
    dependencies = [SearchDependencies::class],
    modules = [SearchModule::class]
)
@SearchFragmentScope
internal interface SearchComponent {
    fun inject(fragment: SearchFragment)

    @Component.Builder
    interface Builder {
        fun dependencies(serverStatusDependencies: SearchDependencies): Builder

        @BindsInstance
        fun context(context: Context): Builder

        @BindsInstance
        fun scope(scope: CoroutineScope): Builder

        @BindsInstance
        fun initialState(initialState: SearchState): Builder

        fun build(): SearchComponent
    }
}

@Module
internal object SearchModule {
    @Provides
    @SearchFragmentScope
    fun provideEpicMiddleware(
        scope: CoroutineScope,
        networkEpic: NetworkRequestEpic,
        favoritesEpic: FavoritesEpic,
        typeRequestEpic: TypeRequestEpic
    ): EpicMiddleware<SearchState, SearchAction> {
        return EpicMiddleware(
            scope,
            listOf(
                networkEpic,
                favoritesEpic,
                typeRequestEpic
            )
        )
    }

    @Provides
    @SearchFragmentScope
    fun provideStore(
        initialState: SearchState,
        epicMiddleware: EpicMiddleware<SearchState, SearchAction>
    ): SearchStore {
        return Store(
            initialState,
            listOf(
                LoggingMiddleware(tag = "SearchState"),
                epicMiddleware
            ),
            SearchState::reduce
        )
    }
}
