package com.primankaden.search.internal.redux.state

import android.os.Parcelable
import com.primankaden.common.network.LoadableData
import com.primankaden.search.internal.network.SearchRequest
import com.primankaden.search.internal.network.SearchResponse
import kotlinx.parcelize.Parcelize

internal sealed class SearchRequestData : LoadableData<SearchRequest, SearchResponse, SearchRequestError> {
    @Parcelize
    data class Loading(override val request: SearchRequest) :
        SearchRequestData(),
        LoadableData.Loading<SearchRequest, SearchResponse, SearchRequestError>

    @Parcelize
    data class Success(override val request: SearchRequest, override val response: SearchResponse) :
        SearchRequestData(),
        LoadableData.Success<SearchRequest, SearchResponse, SearchRequestError>

    @Parcelize
    data class Error(override val request: SearchRequest, override val error: SearchRequestError) :
        SearchRequestData(),
        LoadableData.Error<SearchRequest, SearchResponse, SearchRequestError>
}

internal sealed class SearchRequestError : Parcelable {
    @Parcelize
    object TermTooShortError : SearchRequestError()

    @Parcelize
    object NetworkError : SearchRequestError()

    @Parcelize
    object UnknownError : SearchRequestError()
}
