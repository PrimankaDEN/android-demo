package com.primankaden.search.internal.redux.epics

import com.primankaden.common.redux.EpicMiddleware
import com.primankaden.search.internal.redux.reducer.SearchAction
import com.primankaden.search.internal.redux.state.SearchState
import javax.inject.Inject

internal class EpicRegisterer @Inject constructor(
    private val epicMiddleware: EpicMiddleware<SearchState, SearchAction>
) {
    fun register() {
        epicMiddleware.registerEpics()
    }
}
