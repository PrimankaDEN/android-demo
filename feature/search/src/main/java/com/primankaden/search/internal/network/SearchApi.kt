package com.primankaden.search.internal.network

import android.os.Parcelable
import com.primankaden.common.network.ApiConfig
import com.primankaden.search.internal.di.SearchFragmentScope
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import javax.inject.Inject

@SearchFragmentScope
internal class SearchApi @Inject constructor(
    private val apiConfig: ApiConfig,
    private val client: HttpClient
) {
    suspend fun search(request: SearchRequest): SearchResponse {
        return client.get("${apiConfig.baseUrl}search/") {
            parameter("search", request.search)
            parameter("categories", request.categories)
        }
    }
}

internal enum class SearchCategories(val tag: String) {
    INVENTORY_TYPE("inventory_type")
}

@Parcelize
internal data class SearchRequest(
    val search: String,
    val categories: String = SearchCategories.INVENTORY_TYPE.tag,
    val language: String = "en",
    val datasource: String = "tranquility",
) : Parcelable

@Parcelize
@Serializable
internal data class SearchResponse(
    @SerialName("inventory_type")
    val inventoryTypeIds: List<Int>
) : Parcelable
