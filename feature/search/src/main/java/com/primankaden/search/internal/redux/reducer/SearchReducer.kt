package com.primankaden.search.internal.redux.reducer

import com.primanakden.eve.domain.Type
import com.primanakden.eve.domain.TypeId
import com.primankaden.common.redux.Action
import com.primankaden.search.internal.network.SearchRequest
import com.primankaden.search.internal.redux.state.FavoriteListState
import com.primankaden.search.internal.redux.state.SearchListState
import com.primankaden.search.internal.redux.state.SearchRequestData
import com.primankaden.search.internal.redux.state.SearchRequestError.TermTooShortError
import com.primankaden.search.internal.redux.state.SearchState
import com.primankaden.search.internal.redux.state.SearchTerm

internal interface SearchAction : Action

internal data class SetSearchTermAction(val term: SearchTerm) : SearchAction
internal object ClearSearchTermAction : SearchAction

internal data class SetSearchRequestResultAction(val result: SearchRequestData) : SearchAction
internal data class SetSearchTypeAction(val typeId: TypeId, val type: Type?) : SearchAction

internal data class SetFavoriteTypesAction(val favorites: Set<TypeId>) : SearchAction
internal data class SetFavoriteTypeAction(val typeId: TypeId, val type: Type?) : SearchAction

internal data class AddFavoriteTypeAction(val id: TypeId) : SearchAction
internal data class RemoveFavoriteTypeAction(val id: TypeId) : SearchAction

internal fun SearchState.reduce(action: SearchAction): SearchState {
    return reduceChanges(action)
}

private fun SearchState.reduceChanges(action: SearchAction): SearchState {
    return SearchState(
        searchState = searchState.reduceSearchState(action).reduceSearchRequest(action),
        favoriteState = favoriteState.reduceFavoriteState(action)
    )
}

private fun SearchListState.reduceSearchState(action: SearchAction): SearchListState {
    return copy(
        searchTerm = searchTerm.reduceSearchTerm(action),
        types = types.reduceSearchTypes(action)
    )
}

private fun SearchTerm?.reduceSearchTerm(action: SearchAction): SearchTerm? {
    return when (action) {
        is SetSearchTermAction -> action.term.ifEmpty { null }
        is ClearSearchTermAction -> null
        else -> this
    }
}

private fun Map<TypeId, Type?>.reduceSearchTypes(action: SearchAction): Map<TypeId, Type?> {
    return when (action) {
        is SetSearchTypeAction -> toMutableMap().apply { set(action.typeId, action.type) }
        else -> this
    }
}

private fun FavoriteListState.reduceFavoriteState(action: SearchAction): FavoriteListState {
    return FavoriteListState(
        favoriteIds = favoriteIds.reduceFavorites(action),
        types = types.reduceFavoriteTypes(action)
    )
}

private fun Set<TypeId>.reduceFavorites(action: SearchAction): Set<TypeId> {
    return when (action) {
        is SetFavoriteTypesAction -> action.favorites
        else -> this
    }
}

private fun Map<TypeId, Type?>.reduceFavoriteTypes(action: SearchAction): Map<TypeId, Type?> {
    return when (action) {
        is SetFavoriteTypesAction -> emptyMap()
        is SetFavoriteTypeAction -> toMutableMap().apply { set(action.typeId, action.type) }
        else -> this
    }
}

private fun SearchListState.reduceSearchRequest(action: SearchAction): SearchListState {
    val minTermLength = 5
    return when {
        searchTerm == null -> SearchListState(null, null, emptyMap())
        action is SetSearchRequestResultAction -> SearchListState(searchTerm, action.result, emptyMap())
        searchTerm == searchRequestData?.request?.search -> this
        searchTerm.length < minTermLength -> SearchListState(
            searchTerm,
            SearchRequestData.Error(
                SearchRequest(searchTerm),
                TermTooShortError
            ),
            emptyMap()
        )
        else -> SearchListState(searchTerm, SearchRequestData.Loading(SearchRequest(searchTerm)), emptyMap())
    }
}
