package com.primankaden.accounting.internal.domain.gateway

import com.primanakden.eve.domain.TypeId
import com.primankaden.accounting.internal.domain.entities.MarketStatistic

internal interface MarketStatisticGateway {
    suspend fun marketStatistic(typeId: TypeId): MarketStatistic?
    suspend fun marketStatistics(typeIds: Set<TypeId>): Map<TypeId, MarketStatistic?>
}
