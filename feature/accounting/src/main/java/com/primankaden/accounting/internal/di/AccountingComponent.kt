package com.primankaden.accounting.internal.di

import com.primankaden.accounting.api.AccountingDependencies
import com.primankaden.accounting.api.AccountingFragment
import com.primankaden.accounting.internal.data.repo.MarketStatisticRepo
import com.primankaden.accounting.internal.domain.gateway.MarketStatisticGateway
import com.primankaden.accounting.internal.presentation.details.T1DetailsViewModel
import com.primankaden.accounting.internal.presentation.summary.SummaryViewModel
import dagger.Binds
import dagger.Component
import dagger.Module
import javax.inject.Scope

@Scope
internal annotation class AccountingScope

@AccountingScope
@Component(
    modules = [AccountingModule::class],
    dependencies = [AccountingDependencies::class]
)
internal interface AccountingComponent {
    fun inject(fragment: AccountingFragment)

    fun summaryViewModel(): SummaryViewModel
    fun detailsViewModel(): T1DetailsViewModel

    @Component.Builder
    interface Builder {
        fun dependencies(dependencies: AccountingDependencies): Builder

        fun build(): AccountingComponent
    }
}

@Module
internal interface AccountingModule {
    @Binds
    fun bindMarketStatisticRepo(impl: MarketStatisticRepo): MarketStatisticGateway
}
