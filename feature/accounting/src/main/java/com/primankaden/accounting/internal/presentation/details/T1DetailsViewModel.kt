package com.primankaden.accounting.internal.presentation.details

import androidx.lifecycle.ViewModel
import com.primanakden.eve.domain.Duration
import com.primanakden.eve.domain.TypeId
import com.primanakden.eve.domain.days
import com.primanakden.eve.domain.formatCost
import com.primanakden.eve.domain.formatPercent
import com.primanakden.eve.domain.formatQuantity
import com.primanakden.eve.domain.formatVolume
import com.primankaden.accounting.internal.domain.entities.production.Production
import com.primankaden.accounting.internal.domain.entities.production.ProductionResource
import com.primankaden.accounting.internal.domain.entities.production.cycles
import com.primankaden.accounting.internal.domain.entities.production.expense
import com.primankaden.accounting.internal.domain.entities.production.income
import com.primankaden.accounting.internal.domain.entities.production.margin
import com.primankaden.accounting.internal.domain.entities.production.materialVolume
import com.primankaden.accounting.internal.domain.entities.production.productVolume
import com.primankaden.accounting.internal.domain.entities.production.profit
import com.primankaden.accounting.internal.domain.usecase.t1.ObserveT1ProductionUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject

internal class T1DetailsViewModel @Inject constructor(
    private val observeT1ProductionUseCase: ObserveT1ProductionUseCase
) : ViewModel() {

    fun state(typeId: TypeId): Flow<T1DetailsState> {
        return observeT1ProductionUseCase(typeId)
            .map { production ->
                if (production == null) return@map T1DetailsState.Error("Not found")

                val duration = 1.days()

                val details = production.run {
                    T1Details(
                        blueprintTitle ?: "Unknown",
                        "1 day",
                        cycles(duration).formatQuantity(),
                        materials(duration),
                        materialVolume(duration).formatVolume(),
                        "Tax",
                        expense(duration).formatCost(),
                        product(duration),
                        productVolume(duration).formatVolume(),
                        "Sell tax",
                        income(duration).formatCost(),
                        profit(duration).formatCost(),
                        profit(30.days()).formatCost(),
                        margin.formatPercent()
                    )
                }
                T1DetailsState.Result(details)
            }
            .onStart { T1DetailsState.Loading }
    }
}

private fun Production.materials(duration: Duration): List<T1MaterialDetails> {
    return productionStage.resources
        .mapNotNull {
            if (it is ProductionResource.Stage) {
                T1MaterialDetails(
                    it.stage.outcomes.firstOrNull()?.typeTitle ?: "Unknown",
                    (it.quantity * cycles(duration)).formatQuantity(),
                    it.stage.expense().formatCost(),
                    (it.stage.expense() * it.quantity * cycles(duration)).formatCost()
                )
            } else null
        }
}

private fun Production.product(duration: Duration): T1MaterialDetails {
    return productionStage.outcomes.firstOrNull()!!.let {
        T1MaterialDetails(
            it.typeTitle ?: "Unknown",
            (it.quantity * cycles(duration)).formatQuantity(),
            "",
            ""
            // saleStage.resources.
        )
    }
}
