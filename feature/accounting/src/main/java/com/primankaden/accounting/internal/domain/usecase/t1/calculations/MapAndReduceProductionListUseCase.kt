package com.primankaden.accounting.internal.domain.usecase.t1.calculations

import com.primanakden.eve.domain.AttributeId
import com.primanakden.eve.domain.Blueprint
import com.primanakden.eve.domain.BlueprintActivityType
import com.primankaden.accounting.api.TypeProvider
import com.primankaden.accounting.internal.domain.entities.production.Production
import com.primankaden.accounting.internal.domain.entities.settings.Efficiencies
import com.primankaden.accounting.internal.domain.entities.settings.Taxes
import com.primankaden.accounting.internal.domain.usecase.market.IsTypeAvailableOnMarketUseCase
import com.primankaden.accounting.internal.domain.usecase.settings.ObserveEfficienciesUseCase
import com.primankaden.accounting.internal.domain.usecase.settings.ObserveTaxesUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.runningFold
import javax.inject.Inject

private const val CHUNK_SIZE = 10

internal class MapAndReduceProductionListUseCase @Inject constructor(
    private val typeProvider: TypeProvider,

    private val getT1AccountingUseCase: GetT1AccountingUseCase,
    private val isTypeAvailableOnMarketUseCase: IsTypeAvailableOnMarketUseCase,
    private val observeTaxesUseCase: ObserveTaxesUseCase,
    private val observeEfficienciesUseCase: ObserveEfficienciesUseCase
) {
    operator fun invoke(blueprintsFlow: Flow<List<Blueprint>>): Flow<Set<Production>> {
        return combine(
            blueprintsFlow,
            observeTaxesUseCase(),
            observeEfficienciesUseCase(),
        ) { blueprints, taxes, efficiencies -> Triple(blueprints, taxes, efficiencies) }
            .flatMapLatest { (blueprints, taxes, efficiencies) ->
                loadAndReduce(blueprints, taxes, efficiencies)
            }
    }

    private fun loadAndReduce(
        blueprints: List<Blueprint>,
        taxes: Taxes,
        efficiencies: Efficiencies
    ): Flow<Set<Production>> {
        return flow {
            val result = mutableSetOf<Production>()
            blueprints
                .chunked(CHUNK_SIZE)
                .map {
                    it
                        .map { loadProduction(it, taxes, efficiencies) }
                        .merge()
                        .runningFold(emptySet<Production>()) { acc, value -> value?.let { acc + it } ?: acc }
                }
                .forEach {
                    it.collect { chunk ->
                        result += chunk
                        emit(result)
                    }
                }
        }
    }

    private fun loadProduction(blueprint: Blueprint, taxes: Taxes, efficiencies: Efficiencies): Flow<Production?> {
        return flow {
            emit(
                when {
                    !blueprint.isT1Production() -> null
                    !isTypeAvailableOnMarketUseCase(blueprint.blueprintId) -> null
                    else -> getT1AccountingUseCase(blueprint, taxes, efficiencies)
                }
            )
        }
    }

    private suspend fun Blueprint.isT1Production(): Boolean {
        val products = activities[BlueprintActivityType.Manufacturing]?.products
        if (products?.size != 1) return false
        val productType = products.firstOrNull()?.let { typeProvider.getType(it.typeId) }
        return productType?.attributeValues?.get(AttributeId.TechLevel) == 1.0
    }
}
