package com.primankaden.accounting.internal.presentation.summary

import androidx.annotation.ColorRes
import com.primanakden.eve.domain.TypeId
import com.primankaden.accounting.internal.domain.entities.view.ProductionSummary

internal sealed class SummaryState {
    internal object Loading : SummaryState()
    internal data class Error(val message: String) : SummaryState()
    internal data class Result(
        val summaryT1: ProductionSummary,
        val items: List<SummaryItem>
    ) : SummaryState()
}

internal data class SummaryItem(
    val blueprintId: TypeId,
    val title: String,
    @ColorRes
    val titleColor: Int,
    val monthProfit: String,
    val margin: String,
    @ColorRes
    val marginColor: Int,
    val marketShare: String,
    @ColorRes
    val shareColor: Int,
)
