package com.primankaden.accounting.internal.presentation.details

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import com.primanakden.eve.domain.TypeId
import com.primankaden.accounting.internal.presentation.details.view.T1DetailsView
import com.primankaden.eve.styles.ErrorView
import com.primankaden.eve.styles.LoadingView

@Composable
internal fun T1DetailsScreen(
    typeId: TypeId,
    viewModel: T1DetailsViewModel
) {
    val viewState = viewModel.state(typeId).collectAsState(initial = T1DetailsState.Loading)
    when (val state = viewState.value) {
        T1DetailsState.Loading -> LoadingView()
        is T1DetailsState.Error -> ErrorView(message = state.message)
        is T1DetailsState.Result -> T1DetailsView(details = state.item)
    }
}
