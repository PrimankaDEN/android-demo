package com.primankaden.accounting.internal.domain.entities.settings

import com.primanakden.eve.domain.Efficiency

internal data class Efficiencies(
    val blueprintMaterialEfficiency: Efficiency,
    val blueprintTimeEfficiency: Efficiency,

    val facilityMaterialEfficiency: Efficiency,
    val facilityTimeEfficiency: Efficiency,

    val skillTimeEfficiency: Efficiency,
)
