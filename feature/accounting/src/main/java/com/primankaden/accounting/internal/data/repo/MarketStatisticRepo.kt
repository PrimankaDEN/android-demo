package com.primankaden.accounting.internal.data.repo

import com.primanakden.eve.domain.TypeId
import com.primankaden.accounting.internal.di.AccountingScope
import com.primankaden.accounting.internal.domain.entities.MarketStatistic
import com.primankaden.accounting.internal.domain.gateway.MarketStatisticGateway
import com.primankaden.common.extensions.containsKeys
import com.primankaden.common.extensions.getByKeys
import com.primankaden.common.extensions.skippedKeys
import com.primankaden.common.tools.checkNotMainThread
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import javax.inject.Inject

@AccountingScope
internal class MarketStatisticRepo @Inject constructor(
    private val api: MarketApi
) : MarketStatisticGateway {
    // TODO db here, error processing
    private val cache = mutableMapOf<TypeId, MarketStatistic>()
    private val mutex = Mutex()

    override suspend fun marketStatistic(typeId: TypeId): MarketStatistic? {
        if (!cache.contains(typeId)) {
            mutex.withLock {
                if (!cache.contains(typeId)) {
                    loadOnIo(typeId)?.let { cache[typeId] = it.mapToStatistic() }
                }
            }
        }
        return cache[typeId]
    }

    override suspend fun marketStatistics(typeIds: Set<TypeId>): Map<TypeId, MarketStatistic?> {
        if (!cache.containsKeys(typeIds)) {
            mutex.withLock {
                cache.skippedKeys(typeIds)
                    .asSequence()
                    .associateWith { id -> loadOnIo(id)?.mapToStatistic() }
                    .entries
                    .filter { (_, s) -> s != null }
                    .forEach { (id, s) -> cache[id] = s!! }
            }
        }
        return cache.getByKeys(typeIds)
    }

    private suspend fun loadOnIo(typeId: TypeId): List<MarketHistory>? {
        return withContext(Dispatchers.IO) {
            load(typeId)
        }
    }

    private suspend fun load(typeId: TypeId): List<MarketHistory>? {
        checkNotMainThread()
        return try {
            api.marketHistory(MarketOrderRequest(typeId.id))
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    private fun List<MarketHistory>.mapToStatistic(): MarketStatistic {
        return MarketStatistic(
            takeLast(30).map { it.average }.average(),
            takeLast(30).map { it.volume }.average().toInt()
        )
    }
}
