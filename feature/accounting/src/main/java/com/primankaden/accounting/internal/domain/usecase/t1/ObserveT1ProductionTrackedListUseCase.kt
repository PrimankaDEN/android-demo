package com.primankaden.accounting.internal.domain.usecase.t1

import com.primankaden.accounting.api.BlueprintProvider
import com.primankaden.accounting.api.TrackedBlueprintProvider
import com.primankaden.accounting.internal.domain.entities.production.Production
import com.primankaden.accounting.internal.domain.usecase.t1.calculations.MapAndReduceProductionListUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.mapLatest
import javax.inject.Inject

internal class ObserveT1ProductionTrackedListUseCase @Inject constructor(
    private val trackedBlueprintProvider: TrackedBlueprintProvider,
    private val blueprintProvider: BlueprintProvider,
    private val mapAndReduceProductionListUseCase: MapAndReduceProductionListUseCase,
) {
    operator fun invoke(): Flow<Set<Production>> {
        return mapAndReduceProductionListUseCase(
            trackedBlueprintProvider.blueprints()
                .mapLatest { ids -> ids.mapNotNull { blueprintProvider.blueprint(it) } }
        )
    }
}
