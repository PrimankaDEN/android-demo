package com.primankaden.accounting.internal.presentation.details

internal sealed class T1DetailsState {
    internal object Loading : T1DetailsState()
    internal data class Error(val message: String) : T1DetailsState()
    internal data class Result(val item: T1Details) : T1DetailsState()
}

internal class T1Details(
    val title: String,
    val interval: String,
    val cycles: String,

    val materials: List<T1MaterialDetails>,
    val materialVolume: String,
    val productionTax: String,
    val expense: String,

    val product: T1MaterialDetails,
    val productVolume: String,
    val sellTax: String,
    val income: String,

    val profit: String,
    val monthProfit: String,
    val margin: String,
)

internal class T1MaterialDetails(
    val title: String,
    val quantity: String,
    val price: String,
    val cost: String
)
