package com.primankaden.accounting.internal.domain.usecase.settings

import com.primankaden.accounting.internal.domain.entities.settings.Filters
import com.primankaden.settings.SettingManager
import com.primankaden.settings.Settings
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import javax.inject.Inject

internal class ObserveFiltersUseCase @Inject constructor(
    private val settingManager: SettingManager
) {
    operator fun invoke(): Flow<Filters> {
        return combine(
            settingManager[Settings.availableProductionLines].observe(),
            settingManager[Settings.availableResearchLines].observe(),
            settingManager[Settings.minMargin].observe(),
            settingManager[Settings.maxMarketShare].observe(),
        ) {
                availableProductionLines,
                availableResearchLines,
                minMargin,
                maxMarketShare,
            ->
            Filters(
                availableProductionLines,
                availableResearchLines,
                minMargin,
                maxMarketShare,
            )
        }
    }
}
