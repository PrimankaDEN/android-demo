package com.primankaden.accounting.internal.domain.entities

import com.primanakden.eve.domain.Cost
import com.primanakden.eve.domain.Quantity

internal class MarketStatistic(
    val average: Cost,
    val volume: Quantity,
)
