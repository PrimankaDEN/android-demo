package com.primankaden.accounting.internal.domain.entities.production

import com.primanakden.eve.domain.days
import com.primankaden.accounting.internal.domain.entities.SortType

internal fun productionComparator(sort: SortType): Comparator<Production> {
    return when (sort) {
        SortType.NAME -> compareBy { it.blueprintTitle }
        SortType.PROFIT -> compareByDescending { it.profit(30.days()) }
        SortType.MARGIN -> compareByDescending { it.margin }
        SortType.SHARE -> compareBy { it.marketShare }
    }
}
