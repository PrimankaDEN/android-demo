package com.primankaden.accounting.internal.domain.entities.settings

import com.primanakden.eve.domain.Quantity
import com.primanakden.eve.domain.Ratio

internal data class Filters(
    val availableProductionLines: Quantity,
    val availableResearchLines: Quantity,
    val minMargin: Ratio,
    val maxMarketShare: Ratio,
)
