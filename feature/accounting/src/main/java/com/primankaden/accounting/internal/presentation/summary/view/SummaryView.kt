package com.primankaden.accounting.internal.presentation.summary.view

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.primanakden.eve.domain.formatCost
import com.primanakden.eve.domain.formatVolume
import com.primankaden.accounting.internal.domain.entities.view.ProductionSummary
import com.primankaden.eve.styles.EveTextStyle

@Composable
internal fun SummaryView(summaryT1: ProductionSummary) {
    Row(modifier = Modifier.padding(all = 8.dp)) {
        Column(
            horizontalAlignment = Alignment.Start,
            modifier = Modifier.weight(1f)
        ) {
            Text(text = "D ${summaryT1.dayProfit.formatCost()}", style = EveTextStyle())
            Text(text = "M ${summaryT1.monthProfit.formatCost()}", style = EveTextStyle())
        }
        Column(
            modifier = Modifier
                .padding(start = 8.dp, end = 8.dp)
                .weight(1f),
            horizontalAlignment = Alignment.End
        ) {
            Text(text = summaryT1.dayExpense.formatCost(), style = EveTextStyle())
        }
        Column(
            horizontalAlignment = Alignment.End,
            modifier = Modifier.weight(1f)
        ) {
            Text(text = "In ${summaryT1.dayMaterialVolume.formatVolume()}", style = EveTextStyle())
            Text(text = "Out ${summaryT1.dayProductVolume.formatVolume()}", style = EveTextStyle())
        }
    }
}

@Preview
@Composable
internal fun SummaryViewPreview() {
    SummaryView(
        summaryT1 = ProductionSummary(
            500_000.0,
            500_000_000.0,
            100_000_000.0,
            1000.0,
            500.0
        )
    )
}
