package com.primankaden.accounting.api

import com.primanakden.eve.domain.Blueprint
import com.primanakden.eve.domain.TypeId

interface BlueprintProvider {
    suspend fun blueprint(blueprintTypeId: TypeId): Blueprint?
    suspend fun blueprints(): Set<Blueprint>
}
