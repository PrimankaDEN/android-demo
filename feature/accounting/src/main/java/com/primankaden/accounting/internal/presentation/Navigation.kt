package com.primankaden.accounting.internal.presentation

import androidx.compose.runtime.Composable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.primanakden.eve.domain.TypeId
import com.primanakden.eve.domain.deserialize
import com.primankaden.accounting.internal.di.AccountingComponent
import com.primankaden.accounting.internal.presentation.details.T1DetailsScreen
import com.primankaden.accounting.internal.presentation.summary.SummaryScreen

@Composable
internal fun Navigation(component: AccountingComponent) {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = Screen.Summary.route) {
        composable(Screen.Summary.route) {
            SummaryScreen(navController, daggerViewModel { component.summaryViewModel() })
        }
        composable(
            route = Screen.Details.route + "/{id}",
            arguments = listOf(navArgument("id") { type = NavType.IntType })
        ) {
            T1DetailsScreen(
                it.arguments?.getInt("id").deserialize(),
                daggerViewModel { component.detailsViewModel() }
            )
        }
    }
}

internal sealed class Screen(val route: String) {
    object Summary : Screen("summary")
    object Details : Screen("details")
}

internal fun Screen.withTypeId(typeId: TypeId) = "$route/${typeId.id}"

@Suppress("UNCHECKED_CAST")
@Composable
inline fun <reified T : ViewModel> daggerViewModel(
    key: String? = null,
    crossinline viewModelInstanceCreator: () -> T
): T = androidx.lifecycle.viewmodel.compose.viewModel(
    modelClass = T::class.java,
    key = key,
    factory = object : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return viewModelInstanceCreator() as T
        }
    }
)
