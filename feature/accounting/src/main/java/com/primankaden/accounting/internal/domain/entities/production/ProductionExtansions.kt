package com.primankaden.accounting.internal.domain.entities.production

import com.primanakden.eve.domain.Cost
import com.primanakden.eve.domain.Duration
import com.primanakden.eve.domain.Quantity
import com.primanakden.eve.domain.Ratio
import com.primanakden.eve.domain.TypeId
import com.primanakden.eve.domain.Volume
import com.primanakden.eve.domain.days
import com.primankaden.common.extensions.firstOfType

internal val Production.time: Duration get() = productionStage.time()
internal fun Production.cycles(duration: Duration): Quantity = (duration / time).toInt()

internal val Production.expense: Cost get() = productionStage.expense()
internal fun Production.expense(duration: Duration): Cost = forInterval(expense, duration)
internal val Production.income: Cost get() = saleStage.income()
internal fun Production.income(duration: Duration): Cost = forInterval(income, duration)
internal val Production.profit: Cost get() = income - expense
internal fun Production.profit(duration: Duration): Cost = forInterval(profit, duration)
internal val Production.margin: Ratio get() = (profit / expense) * 100

internal val Production.materialVolume: Volume get() = productionStage.materialVolume()
internal fun Production.materialVolume(duration: Duration): Volume = forInterval(materialVolume, duration)
internal val Production.productVolume: Volume get() = saleStage.productVolume()
internal fun Production.productVolume(duration: Duration): Volume = forInterval(productVolume, duration)

internal val Production.marketShare: Ratio?
    get() {
        return productionStage.outcomes.firstOrNull()?.typeId?.let {
            marketShare(it)
        }
    }

internal fun Production.marketShare(typeId: TypeId): Ratio? {
    return saleStage.firstOrNull { it.typeId == typeId }?.let {
        (it.quantity.toDouble() * cycles(30.days())) / it.resources.firstOfType<SaleResource.MarketVolume>().monthVolume
    }
}

private fun Production.forInterval(value: Double, duration: Duration): Double {
    return value * (duration / time)
}

private fun Production.forInterval(value: Int, duration: Duration): Int {
    return value * (duration / time).toInt()
}

internal fun ProductionStage.time(): Duration {
    return (resources.map { it.time() }.maxOrNull() ?: 0)
}

private fun ProductionResource.time(): Duration {
    return when (this) {
        is ProductionResource.Time -> time
        is ProductionResource.Stage -> stage.time() * quantity
        else -> 0
    }
}

internal fun ProductionStage.expense(): Cost {
    return resources.sumOf { it.expense() }
}

private fun ProductionResource.expense(): Cost {
    return when (this) {
        is ProductionResource.Expense -> cost
        is ProductionResource.Tax -> cost
        is ProductionResource.Stage -> quantity * stage.expense()
        else -> 0.0
    }
}

private fun Set<SaleStage>.income(): Cost {
    return sumOf { product -> product.resources.sumOf { it.income() } * product.quantity }
}

private fun SaleResource.income(): Cost {
    return when (this) {
        is SaleResource.Income -> cost
        is SaleResource.SalesTax -> -cost
        is SaleResource.BrokerFee -> -cost
        else -> 0.0
    }
}

internal fun ProductionStage.materialVolume(): Volume {
    return resources.sumOf { it.materialVolume() }
}

private fun ProductionResource.materialVolume(): Cost {
    return when (this) {
        is ProductionResource.Delivery -> volume
        is ProductionResource.Stage -> quantity * stage.materialVolume()
        else -> 0.0
    }
}

internal fun Set<SaleStage>.productVolume(): Volume {
    return sumOf { product ->
        product.resources.filterIsInstance<SaleResource.Delivery>().sumOf { it.volume } * product.quantity
    }
}
