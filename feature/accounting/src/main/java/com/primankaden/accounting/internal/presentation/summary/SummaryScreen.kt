package com.primankaden.accounting.internal.presentation.summary

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.navigation.NavController
import com.primankaden.accounting.internal.presentation.Screen
import com.primankaden.accounting.internal.presentation.summary.view.SummaryItemView
import com.primankaden.accounting.internal.presentation.summary.view.SummaryTitleView
import com.primankaden.accounting.internal.presentation.summary.view.SummaryView
import com.primankaden.accounting.internal.presentation.withTypeId
import com.primankaden.eve.styles.ErrorView
import com.primankaden.eve.styles.LoadingView

@Composable
internal fun SummaryScreen(
    navController: NavController,
    viewModel: SummaryViewModel
) {
    val viewState = viewModel.state.collectAsState(initial = SummaryState.Loading)

    when (val state = viewState.value) {
        SummaryState.Loading -> LoadingView()
        is SummaryState.Error -> ErrorView(state.message)
        is SummaryState.Result -> SummaryResult(state, navController, viewModel)
    }
}

@Composable
private fun SummaryResult(
    state: SummaryState.Result,
    navController: NavController,
    viewModel: SummaryViewModel,
) {
    Column {
        SummaryView(summaryT1 = state.summaryT1)
        SummaryTitleView(viewModel::onSortClick)
        LazyColumn {
            items(
                items = state.items,
                itemContent = {
                    SummaryItemView(it) { item ->
                        navController.navigate(Screen.Details.withTypeId(item.blueprintId))
                    }
                },
            )
        }
    }
}
