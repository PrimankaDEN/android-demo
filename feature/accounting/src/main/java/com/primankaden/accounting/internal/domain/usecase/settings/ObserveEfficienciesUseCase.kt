package com.primankaden.accounting.internal.domain.usecase.settings

import com.primankaden.accounting.internal.domain.entities.settings.Efficiencies
import com.primankaden.settings.SettingManager
import com.primankaden.settings.Settings
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import javax.inject.Inject

internal class ObserveEfficienciesUseCase @Inject constructor(
    private val settingManager: SettingManager
) {
    operator fun invoke(): Flow<Efficiencies> {
        return combine(
            settingManager[Settings.blueprintMaterialEfficiency].observe(),
            settingManager[Settings.blueprintTimeEfficiency].observe(),
            settingManager[Settings.facilityMaterialEfficiency].observe(),
            settingManager[Settings.facilityTimeEfficiency].observe(),
            settingManager[Settings.skillTimeEfficiency].observe(),
        ) {
                blueprintMaterialEfficiency,
                blueprintTimeEfficiency,
                facilityMaterialEfficiency,
                facilityTimeEfficiency,
                skillTimeEfficiency,
            ->
            Efficiencies(
                blueprintMaterialEfficiency,
                blueprintTimeEfficiency,
                facilityMaterialEfficiency,
                facilityTimeEfficiency,
                skillTimeEfficiency,
            )
        }
    }
}
