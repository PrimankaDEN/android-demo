package com.primankaden.accounting.internal.presentation.details.view

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.primankaden.accounting.internal.presentation.details.T1Details
import com.primankaden.eve.styles.EveSmallTextStyle
import com.primankaden.eve.styles.EveTextStyle

@Composable
internal fun T1DetailsView(details: T1Details) {
    Column {
        Text(
            text = details.title,
            style = EveTextStyle()
        )
        Text(
            text = "Interval ${details.interval}",
            style = EveTextStyle()
        )
        Text(
            text = "Cycles ${details.cycles}",
            style = EveTextStyle()
        )
        details.materials.forEachIndexed { index, it ->
            T1MaterialDetailsView((index + 1).toString(), it)
        }
        Text(
            text = "Volume ${details.materialVolume}",
            style = EveSmallTextStyle()
        )
        Text(
            text = "Production tax ${details.productionTax}",
            style = EveTextStyle()
        )
        Text(
            text = "Expense ${details.expense}",
            style = EveTextStyle().copy(color = Color.Red.copy(alpha = 0.8f))
        )

        T1MaterialDetailsView("Product", details.product)

        Text(
            text = "Volume ${details.productVolume}",
            style = EveSmallTextStyle()
        )
        Text(
            text = "Sell tax ${details.sellTax}",
            style = EveTextStyle()
        )
        Text(
            text = "Income ${details.income}",
            style = EveTextStyle().copy(color = Color.Green.copy(alpha = 0.8f))
        )

        Spacer(modifier = Modifier.padding(bottom = 18.dp))

        Text(
            text = "Profit ${details.profit}",
            style = EveTextStyle().copy(color = Color.Green)
        )
        Text(
            text = "Month profit ${details.monthProfit}",
            style = EveTextStyle().copy(color = Color.Green)
        )

        Text(
            text = "Margin ${details.margin}",
            style = EveTextStyle().copy(color = Color.Green)
        )
    }
}
