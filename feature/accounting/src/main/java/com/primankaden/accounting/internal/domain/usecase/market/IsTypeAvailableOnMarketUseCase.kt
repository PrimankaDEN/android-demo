package com.primankaden.accounting.internal.domain.usecase.market

import com.primanakden.eve.domain.TypeId
import com.primankaden.accounting.internal.domain.gateway.MarketStatisticGateway
import javax.inject.Inject

internal class IsTypeAvailableOnMarketUseCase @Inject constructor(
    private val marketStatisticGateway: MarketStatisticGateway
) {
    suspend operator fun invoke(typeId: TypeId): Boolean {
        return marketStatisticGateway.marketStatistic(typeId)?.volume ?: 0 > 0
    }
}
