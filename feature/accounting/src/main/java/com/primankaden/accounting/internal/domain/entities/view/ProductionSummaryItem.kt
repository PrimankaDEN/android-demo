package com.primankaden.accounting.internal.domain.entities.view

import com.primankaden.accounting.internal.domain.entities.SortType
import com.primankaden.accounting.internal.domain.entities.production.Production
import com.primankaden.accounting.internal.domain.entities.production.productionComparator

internal class ProductionSummaryItem(
    val production: Production,
    val marginAcceptable: Boolean,
    val marketShareAcceptable: Boolean,
    val blueprintAcceptable: Boolean,
) {
    companion object {
        internal fun comparator(sort: SortType): Comparator<ProductionSummaryItem> {
            return compareBy(productionComparator(sort)) { it.production }
        }
    }
}
