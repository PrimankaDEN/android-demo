package com.primankaden.accounting.internal.presentation.summary

import androidx.lifecycle.ViewModel
import com.primanakden.eve.domain.days
import com.primanakden.eve.domain.formatCost
import com.primanakden.eve.domain.formatPercent
import com.primankaden.accounting.R
import com.primankaden.accounting.internal.domain.entities.SortType
import com.primankaden.accounting.internal.domain.entities.production.margin
import com.primankaden.accounting.internal.domain.entities.production.marketShare
import com.primankaden.accounting.internal.domain.entities.production.profit
import com.primankaden.accounting.internal.domain.entities.view.ProductionSummaryItem
import com.primankaden.accounting.internal.domain.usecase.summary.CalculateAccountingSummaryUseCase
import com.primankaden.accounting.internal.domain.usecase.summary.ObserveT1BlueprintSummariesUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject

internal class SummaryViewModel @Inject constructor(
    observeT1BlueprintSummariesUseCase: ObserveT1BlueprintSummariesUseCase,
    calculateAccountingSummaryUseCase: CalculateAccountingSummaryUseCase,
) : ViewModel() {
    private val sortTypes = MutableStateFlow(SortType.PROFIT)

    val state: Flow<SummaryState> = observeT1BlueprintSummariesUseCase()
        .combine(sortTypes) { blueprints, sortType ->
            blueprints.sortedWith(ProductionSummaryItem.comparator(sortType))
        }
        .map { blueprints ->
            val items = blueprints.map {
                val textColor = if (it.blueprintAcceptable) R.color.eve_gray else R.color.eve_gray_dark
                SummaryItem(
                    it.production.blueprintId,
                    it.production.blueprintTitle ?: "Loading",
                    textColor,
                    it.production.profit(30.days()).formatCost(),
                    it.production.margin.formatPercent(),
                    if (it.marginAcceptable) textColor else R.color.eve_red_dark,
                    it.production.marketShare.formatPercent(),
                    if (it.marketShareAcceptable) textColor else R.color.eve_red_dark,
                )
            }
            if (items.isEmpty()) {
                return@map SummaryState.Error("No items")
            }

            val summary = calculateAccountingSummaryUseCase(
                blueprints
                    .filter { it.blueprintAcceptable }
                    .map { it.production }
            )

            SummaryState.Result(summary, items)
        }
        .onStart { SummaryState.Loading }

    fun onSortClick(sortType: SortType) {
        sortTypes.tryEmit(sortType)
    }
}
