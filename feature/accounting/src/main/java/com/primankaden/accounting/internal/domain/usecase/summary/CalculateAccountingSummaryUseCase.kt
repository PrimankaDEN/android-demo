package com.primankaden.accounting.internal.domain.usecase.summary

import com.primanakden.eve.domain.days
import com.primankaden.accounting.internal.domain.entities.production.Production
import com.primankaden.accounting.internal.domain.entities.production.expense
import com.primankaden.accounting.internal.domain.entities.production.materialVolume
import com.primankaden.accounting.internal.domain.entities.production.productVolume
import com.primankaden.accounting.internal.domain.entities.production.profit
import com.primankaden.accounting.internal.domain.entities.view.ProductionSummary
import javax.inject.Inject

internal class CalculateAccountingSummaryUseCase @Inject constructor() {
    operator fun invoke(items: List<Production>): ProductionSummary {
        return ProductionSummary(
            items.sumOf { it.profit(1.days()) },
            items.sumOf { it.profit(30.days()) },
            items.sumOf { it.expense(1.days()) },
            items.sumOf { it.materialVolume(1.days()) },
            items.sumOf { it.productVolume(1.days()) },
        )
    }
}
