package com.primankaden.accounting.internal.domain.usecase.t1.calculations

import com.primanakden.eve.domain.Blueprint
import com.primanakden.eve.domain.BlueprintActivityType
import com.primanakden.eve.domain.TypeId
import com.primankaden.accounting.api.TypeProvider
import com.primankaden.accounting.internal.domain.entities.production.Production
import com.primankaden.accounting.internal.domain.entities.settings.Efficiencies
import com.primankaden.accounting.internal.domain.entities.settings.Taxes
import com.primankaden.accounting.internal.domain.gateway.MarketStatisticGateway
import javax.inject.Inject

internal class GetT1AccountingUseCase @Inject constructor(
    private val typeProvider: TypeProvider,
    private val marketStatisticGateway: MarketStatisticGateway,
    private val calculateT1ProductionUseCase: CalculateT1ProductionUseCase,
) {
    suspend operator fun invoke(blueprint: Blueprint, taxes: Taxes, efficiencies: Efficiencies): Production? {
        val manufacturing = blueprint.activities[BlueprintActivityType.Manufacturing] ?: return null
        val typeIds = mutableSetOf<TypeId>().apply {
            add(blueprint.blueprintId)
            addAll(manufacturing.materials.map { it.typeId })
            addAll(manufacturing.products.map { it.typeId })
        }
        val types = typeProvider.getTypes(typeIds)
        val marketStatistics = marketStatisticGateway.marketStatistics(typeIds)
        val costs = marketStatistics.mapValues { it.value?.average }
        val volumes = marketStatistics.mapValues { it.value?.volume }
        return calculateT1ProductionUseCase(blueprint, taxes, efficiencies, types, costs, volumes)
    }
}
