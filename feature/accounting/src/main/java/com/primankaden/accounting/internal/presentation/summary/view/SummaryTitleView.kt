package com.primankaden.accounting.internal.presentation.summary.view

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.primankaden.accounting.R
import com.primankaden.accounting.internal.domain.entities.SortType
import com.primankaden.eve.styles.EveSmallTextStyle

@Composable
internal fun SummaryTitleView(onSortClick: (SortType) -> Unit) {
    val style = EveSmallTextStyle().copy(color = colorResource(R.color.eve_blue_light))
    Row(
        Modifier.padding(start = 8.dp, end = 8.dp, top = 4.dp, bottom = 4.dp)
    ) {
        Text(
            text = "Item",
            style = style,
            modifier = Modifier
                .width(IntrinsicSize.Max)
                .weight(1f)
                .padding(end = 4.dp)
                .clickable { onSortClick(SortType.NAME) },
            textAlign = TextAlign.Start
        )
        Text(
            text = "Profit",
            style = style,
            modifier = Modifier
                .padding(end = 4.dp)
                .clickable { onSortClick(SortType.PROFIT) },
            textAlign = TextAlign.End,
        )
        Text(
            text = "Margin",
            style = style,
            modifier = Modifier
                .width(56.dp)
                .clickable { onSortClick(SortType.MARGIN) },
            textAlign = TextAlign.End
        )
        Text(
            text = "Share",
            style = style,
            modifier = Modifier
                .width(56.dp)
                .clickable { onSortClick(SortType.SHARE) },
            textAlign = TextAlign.End
        )
    }
}
