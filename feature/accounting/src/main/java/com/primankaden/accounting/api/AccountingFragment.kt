package com.primankaden.accounting.api

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import com.primankaden.accounting.R
import com.primankaden.accounting.internal.di.AccountingComponent
import com.primankaden.accounting.internal.di.DaggerAccountingComponent
import com.primankaden.accounting.internal.presentation.Navigation
import com.primankaden.common.di.findComponentDependencies
import com.primankaden.common.tools.RefWatcher
import javax.inject.Inject

class AccountingFragment : Fragment() {
    internal val component: AccountingComponent by lazy {
        DaggerAccountingComponent.builder()
            .dependencies(findComponentDependencies())
            .build()
    }

    @Inject
    internal lateinit var watcher: RefWatcher

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return LayoutInflater.from(requireContext()).inflate(R.layout.fragment_accounting, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        component.inject(this)
        view.findViewById<ComposeView>(R.id.accounting_compose).setContent { Navigation(component) }
    }

    override fun onDestroy() {
        super.onDestroy()
        watcher.watchRef(component)
    }
}
