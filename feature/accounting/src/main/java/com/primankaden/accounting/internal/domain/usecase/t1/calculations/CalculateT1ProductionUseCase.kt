package com.primankaden.accounting.internal.domain.usecase.t1.calculations

import com.primanakden.eve.domain.Blueprint
import com.primanakden.eve.domain.BlueprintActivityType
import com.primanakden.eve.domain.BlueprintMaterial
import com.primanakden.eve.domain.Cost
import com.primanakden.eve.domain.Quantity
import com.primanakden.eve.domain.Type
import com.primanakden.eve.domain.TypeId
import com.primanakden.eve.domain.tax
import com.primanakden.eve.domain.withEfficiency
import com.primankaden.accounting.internal.domain.entities.production.Production
import com.primankaden.accounting.internal.domain.entities.production.ProductionOutcome
import com.primankaden.accounting.internal.domain.entities.production.ProductionResource
import com.primankaden.accounting.internal.domain.entities.production.ProductionStage
import com.primankaden.accounting.internal.domain.entities.production.SaleResource
import com.primankaden.accounting.internal.domain.entities.production.SaleStage
import com.primankaden.accounting.internal.domain.entities.settings.Efficiencies
import com.primankaden.accounting.internal.domain.entities.settings.Taxes
import com.primankaden.common.extensions.getValuesByKeys
import javax.inject.Inject

internal class CalculateT1ProductionUseCase @Inject constructor() {
    operator fun invoke(
        blueprint: Blueprint,
        taxes: Taxes,
        efficiencies: Efficiencies,
        preloadedTypes: Map<TypeId, Type?>,
        preloadedMarketPrices: Map<TypeId, Cost?>,
        preloadedMarketVolume: Map<TypeId, Quantity?>
    ): Production {
        val purchase: (BlueprintMaterial) -> ProductionResource = { material: BlueprintMaterial ->
            ProductionResource.Stage(
                quantity = material.quantity.withEfficiency(efficiencies.blueprintMaterialEfficiency),
                ProductionStage(
                    resources = setOfNotNull(
                        preloadedMarketPrices[material.typeId]?.let { ProductionResource.Expense(it) },
                        preloadedTypes[material.typeId]?.volume?.let { ProductionResource.Delivery(it) },
                    ),
                    outcomes = setOf(ProductionOutcome(material.typeId, preloadedTypes[material.typeId]?.title))
                )
            )
        }

        val manufacturing = blueprint.activities[BlueprintActivityType.Manufacturing]!!
        val materials = manufacturing.materials
        val products = manufacturing.products

        val productionStage = ProductionStage(
            resources = mutableSetOf<ProductionResource>().apply {
                addAll(materials.map { purchase(it) })
                add(
                    ProductionResource.Tax(
                        preloadedMarketPrices
                            .getValuesByKeys(materials.map { it.typeId })
                            .filterNotNull()
                            .sum()
                            .tax(taxes.facilityProductionTax)
                    )
                )
                add(ProductionResource.Time(manufacturing.time.withEfficiency(efficiencies.blueprintTimeEfficiency)))
                add(ProductionResource.ProductionLine())
                add(ProductionResource.Blueprint(blueprint.blueprintId))
            },
            outcomes = products.map {
                ProductionOutcome(
                    it.typeId,
                    preloadedTypes[it.typeId]?.title,
                    it.quantity,
                    it.probability
                )
            }.toSet()
        )
        val saleStage = products
            .map { product ->
                val price = preloadedMarketPrices[product.typeId]
                val productType = preloadedTypes[product.typeId]
                val share = preloadedMarketVolume[product.typeId]
                SaleStage(
                    product.typeId,
                    productType?.title,
                    product.quantity,
                    setOfNotNull(
                        price?.let { SaleResource.Income(it) },
                        price?.let { SaleResource.SalesTax(it.tax(taxes.salesTax)) },
                        price?.let { SaleResource.BrokerFee(it.tax(taxes.brokerFee)) },
                        productType?.volume?.let { SaleResource.Delivery(it) },
                        share?.let { SaleResource.MarketVolume(it) }
                    )
                )
            }
            .toSet()
        return Production(
            blueprint.blueprintId,
            preloadedTypes[blueprint.blueprintId]?.title,
            productionStage,
            saleStage
        )
    }
}
