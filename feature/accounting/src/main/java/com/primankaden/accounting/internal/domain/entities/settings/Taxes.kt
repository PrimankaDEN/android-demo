package com.primankaden.accounting.internal.domain.entities.settings

import com.primanakden.eve.domain.Tax

internal data class Taxes(
    val facilityProductionTax: Tax,
    val brokerFee: Tax,
    val salesTax: Tax,
)
