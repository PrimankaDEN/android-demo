package com.primankaden.accounting.internal.domain.usecase.t1

import com.primanakden.eve.domain.TypeId
import com.primankaden.accounting.api.BlueprintProvider
import com.primankaden.accounting.internal.domain.entities.production.Production
import com.primankaden.accounting.internal.domain.usecase.settings.ObserveEfficienciesUseCase
import com.primankaden.accounting.internal.domain.usecase.settings.ObserveTaxesUseCase
import com.primankaden.accounting.internal.domain.usecase.t1.calculations.GetT1AccountingUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

internal class ObserveT1ProductionUseCase @Inject constructor(
    private val blueprintProvider: BlueprintProvider,
    private val observeTaxesUseCase: ObserveTaxesUseCase,
    private val observeEfficienciesUseCase: ObserveEfficienciesUseCase,
    private val getT1AccountingUseCase: GetT1AccountingUseCase,
) {
    operator fun invoke(blueprintId: TypeId): Flow<Production?> {
        return combine(
            flow { emit(blueprintProvider.blueprint(blueprintId)) },
            observeTaxesUseCase(),
            observeEfficienciesUseCase()
        ) { blueprint, taxes, efficiencies ->
            blueprint?.let { getT1AccountingUseCase(it, taxes, efficiencies) }
        }
    }
}
