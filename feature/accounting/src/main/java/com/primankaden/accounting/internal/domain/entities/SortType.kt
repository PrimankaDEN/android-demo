package com.primankaden.accounting.internal.domain.entities

internal enum class SortType { NAME, PROFIT, MARGIN, SHARE }
