package com.primankaden.accounting.internal.presentation.details.view

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.primankaden.accounting.internal.presentation.details.T1MaterialDetails
import com.primankaden.eve.styles.EveSmallTextStyle

@Composable
internal fun T1MaterialDetailsView(prefix: String, details: T1MaterialDetails) {
    Row(modifier = Modifier.padding(top = 4.dp, bottom = 4.dp)) {
        Text(
            prefix,
            modifier = Modifier.padding(start = 8.dp),
            style = EveSmallTextStyle()
        )
        Text(
            details.title,
            modifier = Modifier.padding(start = 8.dp),
            style = EveSmallTextStyle()
        )
        Text(
            details.quantity,
            modifier = Modifier.padding(start = 8.dp),
            style = EveSmallTextStyle()
        )
        Text(
            details.price,
            modifier = Modifier.padding(start = 8.dp),
            style = EveSmallTextStyle()
        )
        Text(
            details.cost,
            modifier = Modifier.padding(start = 8.dp, end = 8.dp),
            style = EveSmallTextStyle()
        )
    }
}

@Preview
@Composable
internal fun MaterialDetailsViewPreview() {
    T1MaterialDetailsView(
        prefix = "1",
        details = T1MaterialDetails("Blueprint", "1320K", "10.5", "1032M")
    )
}
