package com.primankaden.accounting.api

import com.primankaden.common.di.ComponentDependencies
import com.primankaden.common.network.ApiConfig
import com.primankaden.common.tools.RefWatcher
import com.primankaden.settings.SettingManager
import io.ktor.client.HttpClient

interface AccountingDependencies : ComponentDependencies {
    val apiConfig: ApiConfig
    val client: HttpClient
    val settingManager: SettingManager
    val blueprintProvider: BlueprintProvider
    val typeProvider: TypeProvider
    val trackedBlueprintProvider: TrackedBlueprintProvider
    val refWatcher: RefWatcher
}
