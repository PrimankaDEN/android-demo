package com.primankaden.accounting.api

import com.primanakden.eve.domain.TypeId
import kotlinx.coroutines.flow.Flow

interface TrackedBlueprintProvider {
    fun blueprints(): Flow<Set<TypeId>>
}
