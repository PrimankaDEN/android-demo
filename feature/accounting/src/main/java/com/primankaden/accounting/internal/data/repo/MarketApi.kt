package com.primankaden.accounting.internal.data.repo

import com.primankaden.accounting.internal.di.AccountingScope
import com.primankaden.common.network.ApiConfig
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import javax.inject.Inject

private const val JITA_SYSTEM_ID = 30000142
private const val JITA_CONSTELLATION_ID = 20000020
private const val JITA_REGION_ID = 10000002

@AccountingScope
internal class MarketApi @Inject constructor(
    private val apiConfig: ApiConfig,
    private val client: HttpClient
) {
    suspend fun marketOrders(request: MarketOrderRequest): List<MarkerOrder> {
        return client.get("${apiConfig.baseUrl}markets/${request.regionId}/orders/") {
            parameter("type_id", request.typeId)
        }
    }

    suspend fun marketHistory(request: MarketOrderRequest): List<MarketHistory> {
        return client.get("${apiConfig.baseUrl}markets/${request.regionId}/history/") {
            parameter("type_id", request.typeId)
        }
    }
}

internal data class MarketOrderRequest(
    val typeId: Int,
    val regionId: Int = JITA_REGION_ID
)

@Serializable
internal data class MarkerOrder(
    @SerialName("is_buy_order")
    val isBuyOrder: Boolean,
    @SerialName("min_volume")
    val minVolume: Int,
    @SerialName("order_id")
    val orderId: Int,
    val price: Int,
    @SerialName("type_id")
    val typeId: Int,
    @SerialName("volume_remain")
    val volumeRemain: Int,
    @SerialName("volume_total")
    val volumeTotal: Int
)

@Serializable
internal data class MarketHistory(
    val average: Double,
    val date: String,
    val highest: Double,
    val lowest: Double,
    @SerialName("order_count")
    val order_count: Long,
    @SerialName("volume")
    val volume: Long
)
