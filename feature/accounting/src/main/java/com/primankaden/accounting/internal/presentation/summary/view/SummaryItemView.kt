package com.primankaden.accounting.internal.presentation.summary.view

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.primanakden.eve.domain.nothing
import com.primankaden.accounting.R
import com.primankaden.accounting.internal.presentation.summary.SummaryItem
import com.primankaden.eve.styles.EveSmallTextStyle

@Composable
internal fun SummaryItemView(item: SummaryItem, onClick: (SummaryItem) -> Unit) {
    Row(
        Modifier
            .padding(start = 8.dp, end = 8.dp, top = 4.dp, bottom = 4.dp)
            .clickable { onClick(item) }
    ) {
        Text(
            text = item.title,
            style = EveSmallTextStyle().copy(color = colorResource(item.titleColor)),
            modifier = Modifier
                .width(IntrinsicSize.Max)
                .weight(1f)
                .padding(end = 4.dp),
            textAlign = TextAlign.Start
        )
        Text(
            text = item.monthProfit,
            style = EveSmallTextStyle().copy(color = colorResource(item.titleColor)),
            modifier = Modifier.padding(end = 4.dp),
            textAlign = TextAlign.End
        )
        Text(
            text = item.margin,
            style = EveSmallTextStyle().copy(color = colorResource(item.marginColor)),
            modifier = Modifier.width(56.dp),
            textAlign = TextAlign.End
        )
        Text(
            text = item.marketShare,
            style = EveSmallTextStyle().copy(color = colorResource(item.shareColor)),
            modifier = Modifier.width(56.dp),
            textAlign = TextAlign.End
        )
    }
}

@Preview
@Composable
internal fun AccountingSummaryItemViewPreview() {
    SummaryItemView(
        SummaryItem(
            nothing(),
            "Rail gun I Blueprint",
            R.color.eve_gray,
            "110.7M",
            "210.78%",
            R.color.eve_gray,
            "130.78%",
            R.color.eve_gray,
        )
    ) {}
}
