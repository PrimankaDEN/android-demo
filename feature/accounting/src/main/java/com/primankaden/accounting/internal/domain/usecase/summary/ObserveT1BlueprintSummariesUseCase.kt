package com.primankaden.accounting.internal.domain.usecase.summary

import com.primankaden.accounting.internal.domain.entities.SortType
import com.primankaden.accounting.internal.domain.entities.production.Production
import com.primankaden.accounting.internal.domain.entities.production.margin
import com.primankaden.accounting.internal.domain.entities.production.marketShare
import com.primankaden.accounting.internal.domain.entities.production.productionComparator
import com.primankaden.accounting.internal.domain.entities.settings.Filters
import com.primankaden.accounting.internal.domain.entities.view.ProductionSummaryItem
import com.primankaden.accounting.internal.domain.usecase.settings.ObserveFiltersUseCase
import com.primankaden.accounting.internal.domain.usecase.t1.ObserveT1ProductionListUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import javax.inject.Inject

internal class ObserveT1BlueprintSummariesUseCase @Inject constructor(
    private val observeT1ProductionListUseCase: ObserveT1ProductionListUseCase,
    private val observeFiltersUseCase: ObserveFiltersUseCase,
) {
    operator fun invoke(): Flow<List<ProductionSummaryItem>> {
        return combine(
            observeT1ProductionListUseCase(),
            observeFiltersUseCase(),
        ) { items, filters ->
            items.filter(filters)
        }
    }

    private fun Set<Production>.filter(filters: Filters): List<ProductionSummaryItem> {
        val lines = filters.availableProductionLines
        val filteredBlueprints = filterNotNull().sortedWith(productionComparator(SortType.PROFIT))

        var acceptableBlueprints = 0
        return filteredBlueprints
            .map {
                val marginAcceptable = it.marginAcceptable(filters)
                val shareAcceptable = it.shareAcceptable(filters)
                if (marginAcceptable && shareAcceptable) {
                    acceptableBlueprints++
                }
                ProductionSummaryItem(
                    it,
                    marginAcceptable,
                    shareAcceptable,
                    marginAcceptable && shareAcceptable && (acceptableBlueprints < lines)
                )
            }
    }

    private fun Production.marginAcceptable(filters: Filters) = margin > filters.minMargin
    private fun Production.shareAcceptable(filters: Filters) = marketShare?.let { it < filters.maxMarketShare } ?: false
}
