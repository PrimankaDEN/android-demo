package com.primankaden.accounting.internal.domain.usecase.settings

import com.primankaden.accounting.internal.domain.entities.settings.Taxes
import com.primankaden.settings.SettingManager
import com.primankaden.settings.Settings
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import javax.inject.Inject

internal class ObserveTaxesUseCase @Inject constructor(
    private val settingManager: SettingManager
) {
    operator fun invoke(): Flow<Taxes> {
        return combine(
            settingManager[Settings.facilityProductionTax].observe(),
            settingManager[Settings.salesTax].observe(),
            settingManager[Settings.brokerFee].observe()
        ) {
                facilityProductionTax,
                salesTax,
                brokerFee,
            ->
            Taxes(
                facilityProductionTax,
                salesTax,
                brokerFee
            )
        }
    }
}
