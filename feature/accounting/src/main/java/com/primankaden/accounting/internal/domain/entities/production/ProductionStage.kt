package com.primankaden.accounting.internal.domain.entities.production

import com.primanakden.eve.domain.Cost
import com.primanakden.eve.domain.Duration
import com.primanakden.eve.domain.Quantity
import com.primanakden.eve.domain.Ratio
import com.primanakden.eve.domain.TypeId
import com.primanakden.eve.domain.Volume

internal data class Production(
    val blueprintId: TypeId,
    val blueprintTitle: String?,
    val productionStage: ProductionStage,
    val saleStage: Set<SaleStage>,
)

internal data class ProductionStage(
    val resources: Set<ProductionResource>,
    val outcomes: Set<ProductionOutcome>,
)

internal sealed class ProductionResource {
    data class ProductionLine(val quantity: Quantity = 1) : ProductionResource()
    data class ResearchLine(val quantity: Quantity = 1) : ProductionResource()
    data class Delivery(val volume: Volume) : ProductionResource()
    data class Tax(val cost: Cost) : ProductionResource()
    data class Expense(val cost: Cost) : ProductionResource()
    data class Blueprint(val blueprintId: TypeId) : ProductionResource()
    data class Time(val time: Duration) : ProductionResource()
    data class Stage(val quantity: Quantity, val stage: ProductionStage) : ProductionResource()
}

internal data class ProductionOutcome(
    val typeId: TypeId,
    val typeTitle: String?,
    val quantity: Quantity = 1,
    val probability: Ratio = 1.0,
)

internal data class SaleStage(
    val typeId: TypeId,
    val typeTitle: String?,
    val quantity: Quantity,
    val resources: Set<SaleResource>,
)

internal sealed class SaleResource {
    data class MarketVolume(val monthVolume: Quantity) : SaleResource()
    data class Income(val cost: Cost) : SaleResource()
    data class SalesTax(val cost: Cost) : SaleResource()
    data class BrokerFee(val cost: Cost) : SaleResource()
    data class Delivery(val volume: Volume) : SaleResource()
}
