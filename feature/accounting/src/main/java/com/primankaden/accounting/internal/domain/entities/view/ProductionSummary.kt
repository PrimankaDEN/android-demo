package com.primankaden.accounting.internal.domain.entities.view

import com.primanakden.eve.domain.Cost
import com.primanakden.eve.domain.Volume

internal data class ProductionSummary(
    val dayProfit: Cost,
    val monthProfit: Cost,
    val dayExpense: Cost,
    val dayMaterialVolume: Volume,
    val dayProductVolume: Volume
)
