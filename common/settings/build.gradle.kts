import dependency.DependencyHolder.Core
import dependency.DependencyHolder.Coroutines
import dependency.DependencyHolder.minSdkVersion
import dependency.DependencyHolder.targetSdkVersion

plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("kotlin-parcelize")
}

externalDependencies {
    setDefaultConfiguration(
        listOf(
            Core.appcompat,
            Core.androidxCore,
            Core.androidxCoreKtx,

            Coroutines.coroutines
        )
    )
}

dependencies {
    implementation(project(":common:common"))
    implementation(project(":common:domain"))
}
