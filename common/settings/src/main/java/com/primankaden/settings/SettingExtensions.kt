package com.primankaden.settings

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.merge

fun Setting<out Any>.changes(): Flow<Unit> = observe().map { }

fun SettingManager.changes(vararg settings: SettingName<out Any>): Flow<Unit> {
    return settings.map { get(it).changes() }.merge()
}

fun SettingManager.resetAll() {
    Settings.settings.forEach { get(it).reset() }
}
