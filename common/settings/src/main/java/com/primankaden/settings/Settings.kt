package com.primankaden.settings

@Suppress("RemoveExplicitTypeArguments")
object Settings {
    private val allSettings = mutableListOf<SettingName<out Any>>()
    val settings: List<SettingName<out Any>> get() = allSettings

    val blueprintMaterialEfficiency = add("blueprintMaterialEfficiency", 10)
    val blueprintTimeEfficiency = add("blueprintTimeEfficiency", 20)

    val facilityMaterialEfficiency = add("facilityMaterialEfficiency", 1)
    val facilityTimeEfficiency = add("facilityTimeEfficiency", 15)

    val skillTimeEfficiency = add("skillTimeEfficiency", 27)

    val facilityProductionTax = add("facilityProductionTax", 3.0)
    val brokerFee = add("brokerFee", 1.77)
    val salesTax = add("salesTax", 4.48)

    val availableProductionLines = add("availableProductionLines", 10)
    val availableResearchLines = add("availableResearchLines", 10)

    val minMargin = add("minMargin", 5.0)
    val maxMarketShare = add("maxMarketShare", 50.0)

    private fun <Type : Any> add(tag: String, defaultValue: Type): SettingName<Type> {
        return SettingName(tag, defaultValue).apply { allSettings.add(this) }
    }
}
