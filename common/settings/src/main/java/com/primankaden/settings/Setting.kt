package com.primankaden.settings

import kotlinx.coroutines.flow.Flow

interface Setting<Type> {
    fun get(): Type
    fun set(type: Type)
    fun reset()

    fun observe(): Flow<Type>
}
