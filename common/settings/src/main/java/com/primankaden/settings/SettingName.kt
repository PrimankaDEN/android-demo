package com.primankaden.settings

data class SettingName<Type>(
    val tag: String,
    internal val defaultValue: Type,
)
