package com.primankaden.settings

interface SettingManager {
    operator fun <Type> get(setting: SettingName<Type>): Setting<Type>
}
