package com.primankaden.common.redux

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

interface Action

typealias Dispatch<A> = suspend ((action: A) -> Unit)

interface Dispatcher<A : Action> {
    fun dispatch(action: A)
}

interface Middleware<S, A : Action> {
    fun interfere(store: Store<S, A>, next: Dispatch<A>): Dispatch<A>
}

class Store<S, A : Action>(
    initialState: S,
    middlewares: List<Middleware<S, A>>,
    val reducer: S.(action: A) -> S
) : Dispatcher<A> {
    private val statesSubject = MutableStateFlow(initialState)
    private val actionSubject = MutableSharedFlow<A>(extraBufferCapacity = 1)
    private val scope: CoroutineScope = MainScope()

    init {
        val reduce: Dispatch<A> = { action ->
            statesSubject.emit(reducer(state(), action))
        }
        val dispatch = middlewares.foldRight(reduce) { middleware, next ->
            middleware.interfere(this, next)
        }
        scope.launch {
            actionSubject.collect { dispatch(it) }
        }
    }

    override fun dispatch(action: A) {
        scope.launch {
            actionSubject.emit(action)
        }
    }

    fun states(): Flow<S> = statesSubject

    fun state(): S = statesSubject.value
}
