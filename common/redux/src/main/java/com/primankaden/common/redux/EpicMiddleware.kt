package com.primankaden.common.redux

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch

interface Epic<S, A : Action> {
    fun act(states: Flow<S>, actions: Flow<A>): Flow<A>
}

class EpicMiddleware<S, A : Action>(
    private val scope: CoroutineScope,
    private val epics: List<Epic<S, A>>,
) : Middleware<S, A> {
    private val actions = MutableSharedFlow<A>(extraBufferCapacity = 1)
    private var store: Store<S, A>? = null

    override fun interfere(store: Store<S, A>, next: Dispatch<A>): Dispatch<A> {
        require(this.store == null) { "You're trying to interfere twice" }
        this.store = store

        return { action ->
            next(action)
            actions.emit(action)
        }
    }

    fun registerEpics() {
        epics.forEach { epic ->
            scope.launch(Dispatchers.Main.immediate) {
                epic.register()
            }
        }
    }

    private suspend fun Epic<S, A>.register() {
        act(requireNotNull(store).states(), actions).collect { action: A ->
            requireNotNull(store).dispatch(action)
        }
    }

    companion object {
        operator fun <S, A : Action> invoke(
            scope: CoroutineScope,
            epic: Epic<S, A>
        ): EpicMiddleware<S, A> = EpicMiddleware(scope, listOf(epic))
    }
}
