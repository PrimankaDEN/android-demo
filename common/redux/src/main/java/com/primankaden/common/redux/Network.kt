package com.primankaden.common.redux

import com.primankaden.common.network.LoadableData

internal sealed interface LoadingResultAction<TResponseSuccess, TResponseError> : Action {
    val request: LoadableData<*, *, *>
//    val response: Response<TResponseSuccess, TResponseError>
}
