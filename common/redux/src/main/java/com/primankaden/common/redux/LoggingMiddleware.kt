package com.primankaden.common.redux

import android.util.Log

class LoggingMiddleware<S, A : Action>(
    private val tag: String = "Store",
    private val printAction: Boolean = true,
    private val printState: Boolean = true
) : Middleware<S, A> {
    override fun interfere(store: Store<S, A>, next: Dispatch<A>): Dispatch<A> {
        return { action ->
            next(action)
            if (printAction) {
                Log.i(tag, "Action $action")
            }
            if (printState) {
                Log.i(tag, "State ${store.state()}")
            }
        }
    }
}
