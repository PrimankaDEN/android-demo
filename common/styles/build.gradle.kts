import dependency.DependencyHolder.Core
import dependency.DependencyHolder.UI

plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("kotlin-kapt")
}

android {
    composeOptions {
        kotlinCompilerExtensionVersion = "1.1.1"
    }
    buildFeatures {
        compose = true
    }
}

externalDependencies {
    setDefaultConfiguration(
        listOf(
            Core.appcompat,
            UI.compose
        )
    )
}
