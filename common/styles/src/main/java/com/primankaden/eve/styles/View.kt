package com.primankaden.eve.styles

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource

@Composable
fun LoadingView() {
    Box(
        Modifier
            .width(IntrinsicSize.Max)
            .height(IntrinsicSize.Max)
            .background(colorResource(id = R.color.eve_blue_dark))
    ) {
        CircularProgressIndicator(
            modifier = Modifier.align(Alignment.Center),
            color = colorResource(id = R.color.eve_blue_light)
        )
    }
}

@Composable
fun ErrorView(message: String) {
    Box(
        Modifier
            .width(IntrinsicSize.Max)
            .height(IntrinsicSize.Max)
    ) {
        Text(
            modifier = Modifier.align(Alignment.Center),
            style = EveTextStyle(),
            text = message,
        )
    }
}
