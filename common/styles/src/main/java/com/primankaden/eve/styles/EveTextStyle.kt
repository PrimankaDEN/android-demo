package com.primankaden.eve.styles

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.sp

@Composable
fun EveTextStyle() = TextStyle(
    color = colorResource(id = R.color.eve_gray),
    fontSize = 16.sp
)

@Composable
fun EveSmallTextStyle() = TextStyle(
    color = colorResource(id = R.color.eve_gray),
    fontSize = 12.sp
)
