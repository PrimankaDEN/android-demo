package com.primanakden.eve.domain

import android.text.format.DateUtils
import kotlin.math.absoluteValue

private const val M = 1_000_000

fun Cost?.formatCost(): String {
    return when {
        this == null -> return "--"
        this.absoluteValue < M -> "%,.1f".format(this)
        else -> "%,.1fM".format(this / M)
    }
}

fun Quantity?.formatQuantity(): String {
    return when {
        this == null -> "--"
        this < M -> "%,d".format(this)
        else -> "%,.1fM".format(this.toDouble() / M)
    }
}

fun Duration?.formatTime(): String {
    return when {
        this == null -> "--.--"
        else -> DateUtils.formatElapsedTime(this)
    }
}

fun Double?.formatPercent(): String {
    return when {
        this == null -> "--%"
        else -> "%,.2f%%".format(this)
    }
}

fun Volume?.formatVolume(): String {
    return when {
        this == null -> "--m3"
        else -> "%,.0fm3".format(this)
    }
}
