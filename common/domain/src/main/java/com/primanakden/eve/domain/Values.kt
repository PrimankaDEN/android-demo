package com.primanakden.eve.domain

/**
 * Time duration in seconds
 * TODO use [kotlin.time.Duration] when it become stable
 */
typealias Duration = Long

/**
 * Quantity of type items
 */
typealias Quantity = Int

/**
 * Cost in ISK
 */
typealias Cost = Double

/**
 * Physical volume of item
 */
typealias Volume = Double

/**
 * Ratio or probability, 0-100
 */
typealias Ratio = Double

/**
 * Material or time efficiency bonus, 0-100
 */
typealias Efficiency = Int

/**
 * Sale tax or fee, 0-100
 */
typealias Tax = Double

/**
 * Some attribute value, format depends on attribute type
 */
typealias AttributeValue = Double

fun Cost.tax(tax: Tax): Cost = (this * tax) / 100
fun Duration.withEfficiency(efficiency: Efficiency): Duration = (this * (1.0 - efficiency / 100)).toLong()
fun Quantity.withEfficiency(efficiency: Efficiency): Quantity = (this * (1.0 - efficiency / 100)).toInt()

fun Int.days(): Duration = this * 24 * 60 * 60L
