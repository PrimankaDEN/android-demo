package com.primanakden.eve.domain

import android.os.Parcelable
import android.util.Log
import kotlinx.parcelize.Parcelize

const val NOTHING_ID = -1

sealed class Id {
    abstract val id: Int
}

@Parcelize
data class TypeId(override val id: Int) : Id(), Parcelable

@Parcelize
data class GroupId(override val id: Int) : Id(), Parcelable

@Parcelize
data class AttributeId(override val id: Int) : Id(), Parcelable {
    companion object {
        val TechLevel: AttributeId = 422.deserialize()
    }
}

fun Id?.isNothing() = this == null || id == NOTHING_ID

inline fun <reified T : Id> nothing(): T = NOTHING_ID.deserialize()

fun Id?.serializeToInt(): Int = this?.id ?: NOTHING_ID

inline fun <reified T : Id> Int?.deserialize(): T {
    val id = this ?: NOTHING_ID
    return when (T::class) {
        TypeId::class -> TypeId(id)
        GroupId::class -> GroupId(id)
        AttributeId::class -> AttributeId(id)
        else -> throw IllegalStateException()
    } as T
}

fun Id?.serializeToString(): String = serializeToInt().toString()

inline fun <reified T : Id> String?.deserialize(): T {
    this ?: return nothing()
    val id: Int = try {
        Integer.parseInt(this)
    } catch (e: NumberFormatException) {
        Log.w("TypeId", "Parse error: ${e.localizedMessage}")
        return nothing()
    }
    return id.deserialize()
}
