package com.primanakden.eve.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

interface Type : Parcelable {
    val typeId: TypeId
    val title: String
    val volume: Volume
    val attributeValues: Map<AttributeId, AttributeValue>
}

@Parcelize
data class RawType(
    override val typeId: TypeId,
    override val title: String,
    override val volume: Volume,
    override val attributeValues: Map<AttributeId, AttributeValue> = emptyMap()
) : Type

@Parcelize
data class TypeWithAttributes(
    private val type: Type,
    val attributes: Map<AttributeId, Attribute?>,
) : Type {
    override val typeId get() = type.typeId
    override val title get() = type.title
    override val volume get() = type.volume
    override val attributeValues get() = type.attributeValues
}
