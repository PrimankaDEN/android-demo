package com.primanakden.eve.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Attribute(
    val attributeId: AttributeId,
    val title: String?,
    val defaultValue: AttributeValue?
) : Parcelable
