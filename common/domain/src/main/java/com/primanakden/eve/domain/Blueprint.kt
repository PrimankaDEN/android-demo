package com.primanakden.eve.domain

data class Blueprint(
    val blueprintId: TypeId,
    val activities: Map<BlueprintActivityType, BlueprintActivity>
)

data class BlueprintActivity(
    val typeBlueprint: BlueprintActivityType,
    val materials: Set<BlueprintMaterial>,
    val products: Set<BlueprintProduct>,
    val time: Duration,
)

enum class BlueprintActivityType {
    Copying, Manufacturing, ResearchMaterials, ResearchTime, Invention, Reaction
}

data class BlueprintMaterial(
    val typeId: TypeId,
    val quantity: Quantity,
)

data class BlueprintProduct(
    val typeId: TypeId,
    val quantity: Quantity,
    val probability: Ratio,
)
