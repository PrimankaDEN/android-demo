package com.primankaden.common.cache

import com.primankaden.common.extensions.containsKeys
import com.primankaden.common.extensions.getByKeys
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import java.util.WeakHashMap

class MemoryCacheRepo<Id, Entity : Any>(
    private val loader: suspend ((Id) -> Entity?)
) {
    private val cache = WeakHashMap<Id, Entity>()
    private val mutex = Mutex()

    suspend fun get(id: Id): Entity? {
        if (!cache.contains(id)) {
            mutex.withLock {
                if (!cache.contains(id)) {
                    cache[id] = loader(id)
                }
            }
        }
        return cache[id]
    }

    suspend fun get(ids: Set<Id>): Map<Id, Entity?> {
        if (!cache.containsKeys(ids)) {
            mutex.withLock {
                ids.forEach { id ->
                    if (!cache.contains(id)) {
                        cache[id] = loader(id)
                    }
                }
            }
        }
        return cache.getByKeys(ids)
    }

    companion object {
        fun <Id, Entity : Any> io(loader: suspend ((Id) -> Entity?)): MemoryCacheRepo<Id, Entity> {
            return MemoryCacheRepo { id ->
                withContext(Dispatchers.IO) {
                    loader(id)
                }
            }
        }
    }
}
