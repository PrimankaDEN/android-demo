package com.primankaden.common.extensions

import androidx.lifecycle.LifecycleCoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map

fun <T> Flow<T>.launchWhenStarted(scope: LifecycleCoroutineScope) {
    scope.launchWhenStarted {
        this@launchWhenStarted.collect()
    }
}

inline fun <reified T> Flow<*>.cast(): Flow<T> = map { it as T }

inline fun <reified T> Flow<Any>.ofType(): Flow<T> {
    return filter { it is T }.map { it as T }
}

fun <T> Flow<T>.zipWithNext(): Flow<Pair<T?, T>> {
    return flow {
        var prev: T? = null
        collect { current ->
            emit(prev to current)
            prev = current
        }
    }
}

fun <T> Flow<T>.skipAll(): Flow<T> = filter { false }
