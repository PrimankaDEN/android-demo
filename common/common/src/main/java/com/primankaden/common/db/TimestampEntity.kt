package com.primankaden.common.db

import java.util.Date
import java.util.concurrent.TimeUnit

interface TimestampEntity {
    var createdAt: Long
    var updatedAt: Long
}

interface TimestampDao<E> {
    fun insert(entity: E)
    fun update(entity: E)
}

fun <E : TimestampEntity> TimestampDao<E>.insertWithTimestamp(entity: E) {
    insert(
        entity.apply {
            createdAt = Date().time
            updatedAt = Date().time
        }
    )
}

fun <E : TimestampEntity> TimestampDao<E>.updateWithTimestamp(entity: E) {
    update(
        entity.apply {
            updatedAt = Date().time
        }
    )
}

fun <E : TimestampEntity> TimestampDao<E>.updateWithTimestamp(entity: E, prevValue: E?) {
    if (prevValue == null) {
        insertWithTimestamp(entity)
    } else {
        updateWithTimestamp(entity)
    }
}

fun TimestampEntity?.isActual(duration: Long, unit: TimeUnit): Boolean {
    return this?.updatedAt?.let { Date().time - it < unit.toMillis(duration) } ?: false
}
