package com.primankaden.common.tools

import android.os.Looper
import androidx.annotation.MainThread

@MainThread
fun checkMainThread() {
    check(isMainThread) {
        "This method must be called from main thread. Current thread is ${Thread.currentThread()}"
    }
}

fun checkNotMainThread() {
    check(!isMainThread) {
        "This method can not be called from main thread. Current thread is ${Thread.currentThread()}"
    }
}

val isMainThread: Boolean
    get() = Looper.myLooper() === Looper.getMainLooper()
