package com.primankaden.common.network

interface ApiConfig {
    val baseUrl: String
    val serverName: String
    val language: String
}
