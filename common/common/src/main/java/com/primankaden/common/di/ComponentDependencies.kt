package com.primankaden.common.di

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.view.View
import androidx.fragment.app.Fragment
import dagger.MapKey
import dagger.Module
import dagger.multibindings.Multibinds
import kotlin.reflect.KClass

// https://habr.com/ru/company/yandex/blog/584756/

interface ComponentDependencies

typealias ComponentDependenciesProvider = Map<Class<out ComponentDependencies>, @JvmSuppressWildcards ComponentDependencies>

interface HasComponentDependencies {
    val dependencies: ComponentDependenciesProvider
}

inline fun <reified T : ComponentDependencies> Application.findComponentDependencies() =
    (this as HasComponentDependencies).dependencies[T::class.java] as? T
        ?: depsNotFound<T>(this)

inline fun <reified T : ComponentDependencies> Context.findComponentDependencies() =
    (this as? HasComponentDependencies)?.dependencies?.get(T::class.java) as? T
        ?: (applicationContext as? Application)?.findComponentDependencies()
        ?: depsNotFound<T>(this)

inline fun <reified T : ComponentDependencies> Fragment.findComponentDependencies() =
    (this as? HasComponentDependencies)?.dependencies?.get(T::class.java) as? T
        ?: activity?.findComponentDependencies()
        ?: depsNotFound<T>(this)

inline fun <reified T : ComponentDependencies> View.findComponentDependencies(): T =
    parents
        .mapNotNull { parent -> (parent as? HasComponentDependencies)?.dependencies?.get(T::class.java) as? T }
        .firstOrNull()
        ?: depsNotFound<T>(parents)

inline fun <reified T : ComponentDependencies> depsNotFound(dependenciesHolder: Any): Nothing =
    throw IllegalStateException("Dependencies ${T::class.java.name} not found in $dependenciesHolder")

val View.parents: Iterable<Any> get() = ViewParentsIterable(this)

private class ViewParentsIterable(private val view: View) : Iterable<Any> {
    override fun iterator() = object : Iterator<Any> {
        @SuppressLint("StaticFieldLeak")
        private var currentView = view
        private var contextChecked = false
        private var applicationChecked = false

        override fun hasNext() = currentView.parent is View ||
            !contextChecked ||
            !applicationChecked

        override fun next(): Any {
            (currentView.parent as? View)?.let {
                currentView = view
                return view
            }
            if (!contextChecked) {
                contextChecked = true
                return currentView.context
            }

            if (!applicationChecked) {
                applicationChecked = true
                return currentView.context.applicationContext
            }
            throw NoSuchElementException("No more parents")
        }
    }
}

@MapKey
@Target(AnnotationTarget.FUNCTION)
annotation class ComponentDependenciesKey(val value: KClass<out ComponentDependencies>)

@Module
interface ComponentDependenciesModule {
    @Multibinds
    fun componentDependencies(): ComponentDependenciesProvider
}
