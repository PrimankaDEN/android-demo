package com.primankaden.common.di

import javax.inject.Qualifier

annotation class RxScheduler {
    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class Io

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class Computation

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class NewThread

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class MainThread
}
