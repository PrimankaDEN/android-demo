package com.primankaden.common.extensions.view

import android.view.View

fun Boolean.toVisibleInvisible() = if (this) View.VISIBLE else View.INVISIBLE
fun <T> T?.toVisibleInvisible() = if (this == null) View.INVISIBLE else View.VISIBLE
fun Boolean.toVisibleGone() = if (this) View.VISIBLE else View.GONE
fun <T> T?.toVisibleGone() = if (this == null) View.GONE else View.VISIBLE
