package com.primankaden.common.network

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
class CommonError : Parcelable
