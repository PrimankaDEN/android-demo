package com.primankaden.common.extensions

import android.os.Binder
import android.os.Bundle
import android.os.Parcelable
import androidx.core.app.BundleCompat
import androidx.fragment.app.Fragment
import kotlin.reflect.KProperty

val Fragment.args: Bundle
    get() {
        if (arguments == null) {
            arguments = Bundle()
        }
        return arguments!!
    }

@Suppress("UNCHECKED_CAST")
operator fun <T> Bundle.getValue(thisRef: Any?, property: KProperty<*>) = get(property.name) as T

operator fun <T> Bundle.setValue(thisRef: Any?, property: KProperty<*>, value: T) {
    val key = property.name
    if (value == null) {
        remove(key)
        return
    }
    when (value) {
        is String -> putString(key, value)
        is Int -> putInt(key, value)
        is Short -> putShort(key, value)
        is Long -> putLong(key, value)
        is Byte -> putByte(key, value)
        is ByteArray -> putByteArray(key, value)
        is Char -> putChar(key, value)
        is CharArray -> putCharArray(key, value)
        is CharSequence -> putCharSequence(key, value)
        is Float -> putFloat(key, value)
        is Boolean -> putBoolean(key, value)
        is Bundle -> putBundle(key, value)
        is Binder -> BundleCompat.putBinder(this, key, value)
        is Parcelable -> putParcelable(key, value)
        is java.io.Serializable -> putSerializable(key, value)
        is Collection<*> -> if (value.firstOrNull() is Parcelable) {
            @Suppress("UNCHECKED_CAST")
            putParcelableArrayList(key, ArrayList(value as Collection<Parcelable>))
        } else {
            throw IllegalStateException("Type of property ${property.name} is not supported")
        }
        else -> throw IllegalStateException(
            "Type ${value.javaClass.canonicalName} of property ${property.name} is not supported"
        )
    }
}
