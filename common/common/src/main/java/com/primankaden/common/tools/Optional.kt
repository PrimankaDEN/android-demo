package com.primankaden.common.tools

data class Optional<out T : Any>(val value: T?) {
    fun isEmpty() = value == null
    fun get() = value!!
}

fun <T> T?.toOptional() = Optional(this)
