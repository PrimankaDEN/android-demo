package com.primankaden.common.extensions

fun <T> List<T>.startWith(item: T): List<T> {
    return toMutableList().apply { add(0, item) }
}

fun <T> Collection<T>.equalsBy(other: Collection<T>?, equalizer: (T, T) -> Boolean): Boolean {
    return other != null && size == other.size && asSequence().zip(other.asSequence(), equalizer).all { it }
}

// map extensions
fun <T> Map<T, Any>.containsKeys(keys: Iterable<T>): Boolean = keys.all { containsKey(it) }
fun <T> Map<T, Any>.skippedKeys(keys: Iterable<T>): Set<T> = keys.filter { !containsKey(it) }.toSet()
fun <T, E> Map<T, E>.getByKeys(keys: Iterable<T>): Map<T, E?> = keys.associateWith { get(it) }
fun <T, E> Map<T, E>.getValuesByKeys(keys: Iterable<T>): List<E> = keys.mapNotNull { get(it) }

// cast extensions
inline fun <reified T> Iterable<*>.ofType(): List<T> = filterIsInstance<T>()
inline fun <reified T> Iterable<*>.firstOfType(): T = ofType<T>().first()
