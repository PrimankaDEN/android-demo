package com.primankaden.common.tools

interface RefWatcher {
    fun watchRef(obj: Any?, reason: String? = null)
}
