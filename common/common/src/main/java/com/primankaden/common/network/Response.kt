package com.primankaden.common.network

import android.os.Parcelable

interface LoadableData<TRequest, TResponse, TError> : Parcelable {
    val request: TRequest

    interface Loading<TRequest, TResponse, TError> : LoadableData<TRequest, TResponse, TError>

    interface Success<TRequest, TResponse, TError> : LoadableData<TRequest, TResponse, TError> {
        val response: TResponse
    }

    interface Error<TRequest, TResponse, TError> : LoadableData<TRequest, TResponse, TError> {
        val error: TError
    }
}
