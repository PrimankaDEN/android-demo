package com.primankaden.androiddemo

import com.primankaden.common.tools.RefWatcher
import leakcanary.AppWatcher
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RefWatcherImpl @Inject constructor() : RefWatcher {
    override fun watchRef(obj: Any?, reason: String?) {
        obj?.let {
            AppWatcher.objectWatcher.expectWeaklyReachable(obj, reason ?: "Unused object")
        }
    }
}
