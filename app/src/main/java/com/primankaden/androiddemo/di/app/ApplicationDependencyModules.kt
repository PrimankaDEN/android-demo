package com.primankaden.androiddemo.di.app

import com.primankaden.eve.sde.api.SdeDependencies
import com.primankaden.eve.sde.api.SdeProvider
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
interface SdeDependencyModule {
    @Binds
    fun bindSdeDependency(component: ApplicationComponent): SdeDependencies

    companion object {
        @Provides
        @Singleton
        fun provideSdeProvider(dependencies: SdeDependencies): SdeProvider {
            return SdeProvider.Builder().dependencies(dependencies).build()
        }
    }
}
