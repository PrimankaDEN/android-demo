package com.primankaden.androiddemo.di.app

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.datastore.core.DataStore
import androidx.datastore.core.handlers.ReplaceFileCorruptionHandler
import androidx.datastore.preferences.core.PreferenceDataStoreFactory
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.preferencesDataStoreFile
import com.primankaden.androiddemo.RefWatcherImpl
import com.primankaden.androiddemo.core.CoreApplication
import com.primankaden.androiddemo.di.activity.ActivityComponentDependencies
import com.primankaden.androiddemo.integration.common.FavoriteGateway
import com.primankaden.androiddemo.integration.common.impl.FavoriteRepo
import com.primankaden.common.di.ComponentDependenciesModule
import com.primankaden.common.network.ApiConfig
import com.primankaden.common.tools.RefWatcher
import com.primankaden.eve.sde.api.BlueprintProvider
import com.primankaden.eve.sde.api.SdeDependencies
import com.primankaden.eve.sde.api.SdeProvider
import com.primankaden.eve.sde.api.TypeProvider
import dagger.Binds
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import io.ktor.client.HttpClient
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.features.logging.LogLevel
import io.ktor.client.features.logging.Logger
import io.ktor.client.features.logging.Logging
import io.ktor.client.features.logging.MessageLengthLimitingLogger
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.serialization.json.Json
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BASIC
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ApplicationModule::class,
        ApplicationBindModule::class,
        ComponentDependenciesModule::class,
        SdeDependencyModule::class,
    ]
)
interface ApplicationComponent : SdeDependencies, ActivityComponentDependencies {
    fun inject(app: CoreApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(context: Application): Builder

        fun build(): ApplicationComponent
    }
}

@Module
object ApplicationModule {
    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().also { it.level = BASIC })
            .build()
    }

    @Provides
    @Singleton
    fun provideKtorHttpClient(): HttpClient {
        return HttpClient().config {
            install(JsonFeature) {
                serializer = KotlinxSerializer(Json { ignoreUnknownKeys = true })
            }
            install(Logging) {
                logger = MessageLengthLimitingLogger(
                    delegate = object : Logger {
                        override fun log(message: String) {
                            Log.i("HttpClient", message)
                        }
                    }
                )
                level = LogLevel.INFO
            }
        }
    }

    @Provides
    @Singleton
    fun provideApiConfig(): ApiConfig {
        return object : ApiConfig {
            override val baseUrl = "https://esi.evetech.net/latest/"
            override val serverName = "tranquility"
            override val language = "en"
        }
    }

    @Provides
    @Singleton
    fun providePreferenceDataStore(context: Context): DataStore<Preferences> {
        val dataStoreFile = "core_data_store"
        return PreferenceDataStoreFactory.create(
            produceFile = { context.preferencesDataStoreFile(dataStoreFile) },
            scope = CoroutineScope(Dispatchers.IO + SupervisorJob()),
            corruptionHandler = ReplaceFileCorruptionHandler(
                produceNewData = { emptyPreferences() }
            ),
        )
    }

    @Provides
    @Singleton
    fun provideTypeProvider(sdeProvider: SdeProvider): TypeProvider = sdeProvider.typeProvider()

    @Provides
    @Singleton
    fun provideBlueprintProvider(sdeProvider: SdeProvider): BlueprintProvider = sdeProvider.blueprintProvider()
}

@Module
interface ApplicationBindModule {
    @Binds
    fun bindFavorites(impl: FavoriteRepo): FavoriteGateway

    @Binds
    fun bindRefWatcher(impl: RefWatcherImpl): RefWatcher

    @Binds
    fun bindContext(app: Application): Context
}
