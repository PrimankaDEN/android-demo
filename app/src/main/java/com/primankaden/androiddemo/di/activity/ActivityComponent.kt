package com.primankaden.androiddemo.di.activity

import android.app.Activity
import android.content.Context
import com.primankaden.accounting.api.AccountingDependencies
import com.primankaden.androiddemo.core.CoreActivity
import com.primankaden.search.api.SearchDependencies
import com.primankaden.serverstatus.api.ServerStatusDependencies
import dagger.Binds
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import javax.inject.Scope

@Scope
annotation class ActivityScope

@ActivityScope
@Component(
    dependencies = [ActivityComponentDependencies::class],
    modules = [
        ActivityBindModule::class,
        AccountingDependencyModule::class,
        ServerStatusDependencyModule::class,
        SearchDependencyModule::class,
    ]
)
interface ActivityComponent : ServerStatusDependencies, SearchDependencies, AccountingDependencies {
    fun inject(activity: CoreActivity)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun activity(activity: Activity): Builder

        fun dependencies(dependencies: ActivityComponentDependencies): Builder

        fun build(): ActivityComponent
    }
}

@Module
interface ActivityBindModule {
    @Binds
    fun bindContext(activity: Activity): Context
}
