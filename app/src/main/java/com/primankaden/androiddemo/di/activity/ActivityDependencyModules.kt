package com.primankaden.androiddemo.di.activity

import com.primankaden.accounting.api.AccountingDependencies
import com.primankaden.accounting.api.BlueprintProvider
import com.primankaden.accounting.api.SettingsProvider
import com.primankaden.accounting.api.TrackedBlueprintProvider
import com.primankaden.androiddemo.integration.module.accounting.BlueprintProviderImpl
import com.primankaden.androiddemo.integration.module.accounting.SettingsProviderImpl
import com.primankaden.androiddemo.integration.module.accounting.TrackedBlueprintProviderImpl
import com.primankaden.androiddemo.integration.module.search.FavoritesProviderImpl
import com.primankaden.androiddemo.integration.module.search.TypeProviderImpl
import com.primankaden.common.di.ComponentDependencies
import com.primankaden.common.di.ComponentDependenciesKey
import com.primankaden.search.api.FavoritesProvider
import com.primankaden.search.api.SearchDependencies
import com.primankaden.search.api.TypeProvider
import com.primankaden.serverstatus.api.ServerStatusDependencies
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface SearchDependencyModule {
    @Binds
    @IntoMap
    @ComponentDependenciesKey(SearchDependencies::class)
    fun bindSearchDependencies(component: ActivityComponent): ComponentDependencies

    @Binds
    fun bindFavoriteProvider(impl: FavoritesProviderImpl): FavoritesProvider

    @Binds
    fun bindTypeProvider(impl: TypeProviderImpl): TypeProvider
}

@Module
interface ServerStatusDependencyModule {
    @Binds
    @IntoMap
    @ComponentDependenciesKey(ServerStatusDependencies::class)
    fun bindServerStatusDependency(component: ActivityComponent): ComponentDependencies
}

@Module
interface AccountingDependencyModule {
    @Binds
    @IntoMap
    @ComponentDependenciesKey(AccountingDependencies::class)
    fun bindServerStatusDependency(component: ActivityComponent): ComponentDependencies

    @Binds
    fun bindBlueprintInfoProvider(impl: BlueprintProviderImpl): BlueprintProvider

    @Binds
    fun bindTypeProvide(impl: com.primankaden.androiddemo.integration.module.accounting.TypeProviderImpl): com.primankaden.accounting.api.TypeProvider

    @Binds
    fun bindSettingsProvider(impl: SettingsProviderImpl): SettingsProvider

    @Binds
    fun bindTrackedItemsProvider(impl: TrackedBlueprintProviderImpl): TrackedBlueprintProvider
}
