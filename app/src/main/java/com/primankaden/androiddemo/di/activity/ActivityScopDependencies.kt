package com.primankaden.androiddemo.di.activity

import com.primankaden.androiddemo.integration.common.FavoriteGateway
import com.primankaden.common.network.ApiConfig
import com.primankaden.common.tools.RefWatcher
import com.primankaden.eve.sde.api.BlueprintProvider
import com.primankaden.eve.sde.api.TypeProvider
import io.ktor.client.HttpClient
import okhttp3.OkHttpClient

interface ActivityComponentDependencies {
    val apiConfig: ApiConfig
    val okHttpClient: OkHttpClient
    val httpClient: HttpClient
    val refWatcher: RefWatcher
    val favoriteGateway: FavoriteGateway
    val typeProvider: TypeProvider
    val blueprintProvider: BlueprintProvider
}
