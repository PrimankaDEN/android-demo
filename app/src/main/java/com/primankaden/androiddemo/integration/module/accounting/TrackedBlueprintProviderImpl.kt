package com.primankaden.androiddemo.integration.module.accounting

import com.primanakden.eve.domain.TypeId
import com.primankaden.accounting.api.TrackedBlueprintProvider
import com.primankaden.androiddemo.di.activity.ActivityScope
import com.primankaden.androiddemo.integration.common.FavoriteGateway
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ActivityScope
class TrackedBlueprintProviderImpl @Inject constructor(
    private val favoriteGateway: FavoriteGateway,
) : TrackedBlueprintProvider {
    override fun blueprints(): Flow<Set<TypeId>> {
        return favoriteGateway.favorites()
    }
}
