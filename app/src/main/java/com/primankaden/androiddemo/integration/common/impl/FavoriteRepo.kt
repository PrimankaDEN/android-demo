package com.primankaden.androiddemo.integration.common.impl

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringSetPreferencesKey
import com.primanakden.eve.domain.TypeId
import com.primanakden.eve.domain.deserialize
import com.primanakden.eve.domain.serializeToString
import com.primankaden.androiddemo.integration.common.FavoriteGateway
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FavoriteRepo @Inject constructor(
    private val store: DataStore<Preferences>
) : FavoriteGateway {
    private val key = stringSetPreferencesKey("favorite_ids")

    override suspend fun add(typeId: TypeId) {
        store.edit {
            val ids = it[key] ?: emptySet()
            it[key] = ids.toMutableSet().apply { add(typeId.serializeToString()) }
        }
    }

    override suspend fun remove(typeId: TypeId) {
        store.edit {
            val ids = it[key] ?: emptySet()
            it[key] = ids.toMutableSet().apply { remove(typeId.serializeToString()) }
        }
    }

    override fun favorites(): Flow<Set<TypeId>> {
        return store.data
            .map { preferences -> preferences[key] ?: emptySet() }
            .map { ids -> ids.asSequence().mapNotNull { it.deserialize<TypeId>() }.toSet() }
            .distinctUntilChanged()
    }
}
