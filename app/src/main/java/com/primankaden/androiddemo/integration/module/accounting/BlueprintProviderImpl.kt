package com.primankaden.androiddemo.integration.module.accounting

import com.primanakden.eve.domain.Blueprint
import com.primanakden.eve.domain.TypeId
import com.primankaden.accounting.api.BlueprintProvider
import javax.inject.Inject

class BlueprintProviderImpl @Inject constructor(
    private val blueprintProvider: com.primankaden.eve.sde.api.BlueprintProvider
) : BlueprintProvider {
    override suspend fun blueprint(blueprintTypeId: TypeId): Blueprint? =
        blueprintProvider.getBlueprint(blueprintTypeId)

    override suspend fun blueprints(): Set<Blueprint> = blueprintProvider.getBlueprints()
}
