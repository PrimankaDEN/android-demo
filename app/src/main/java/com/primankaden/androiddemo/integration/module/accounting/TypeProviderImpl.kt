package com.primankaden.androiddemo.integration.module.accounting

import com.primanakden.eve.domain.Type
import com.primanakden.eve.domain.TypeId
import com.primankaden.eve.sde.api.TypeProvider
import javax.inject.Inject

class TypeProviderImpl @Inject constructor(
    private val typeProvider: TypeProvider
) : com.primankaden.accounting.api.TypeProvider {
    override suspend fun getType(typeId: TypeId): Type? = typeProvider.getType(typeId)

    override suspend fun getTypes(typeIds: Set<TypeId>): Map<TypeId, Type?> = typeProvider.getTypes(typeIds)
}
