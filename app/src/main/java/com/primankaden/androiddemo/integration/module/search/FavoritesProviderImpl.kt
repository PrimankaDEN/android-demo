package com.primankaden.androiddemo.integration.module.search

import com.primanakden.eve.domain.TypeId
import com.primankaden.androiddemo.di.activity.ActivityScope
import com.primankaden.androiddemo.integration.common.FavoriteGateway
import com.primankaden.search.api.FavoritesProvider
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ActivityScope
class FavoritesProviderImpl @Inject constructor(
    private val favoriteGateway: FavoriteGateway
) : FavoritesProvider {
    override suspend fun addFavorite(id: TypeId) {
        favoriteGateway.add(id)
    }

    override suspend fun removeFavorite(id: TypeId) {
        favoriteGateway.remove(id)
    }

    override fun favorites(): Flow<Set<TypeId>> {
        return favoriteGateway.favorites()
    }
}
