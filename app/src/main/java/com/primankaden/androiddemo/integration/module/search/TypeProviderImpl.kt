package com.primankaden.androiddemo.integration.module.search

import com.primanakden.eve.domain.Type
import com.primanakden.eve.domain.TypeId
import com.primankaden.eve.sde.api.TypeProvider
import javax.inject.Inject

class TypeProviderImpl @Inject constructor(
    private val typeProvider: TypeProvider
) : com.primankaden.search.api.TypeProvider {
    override suspend fun getType(id: TypeId): Type? = typeProvider.getType(id)
}
