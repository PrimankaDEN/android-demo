package com.primankaden.androiddemo.integration.common

import com.primanakden.eve.domain.TypeId
import kotlinx.coroutines.flow.Flow

interface FavoriteGateway {
    suspend fun add(typeId: TypeId)
    suspend fun remove(typeId: TypeId)
    fun favorites(): Flow<Set<TypeId>>
}
