package com.primankaden.androiddemo.core

import android.app.Application
import com.primankaden.androiddemo.di.app.ApplicationComponent
import com.primankaden.androiddemo.di.app.DaggerApplicationComponent
import com.primankaden.common.di.ComponentDependenciesProvider
import com.primankaden.common.di.HasComponentDependencies
import javax.inject.Inject

class CoreApplication : Application(), HasComponentDependencies {
    @Inject
    override lateinit var dependencies: ComponentDependenciesProvider

    val component: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder().application(this).build()
    }

    override fun onCreate() {
        super.onCreate()
        component.inject(this)
    }
}
